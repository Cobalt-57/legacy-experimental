Placeholder Readme!

So, to put everything into perspective, I should explain everything here, shouldn't I?

I'm currently compiling everything under Visual Studio 2008 with Source Base 2006.
I am using Half-Life: Source Legacy as a base as it is currently the most functioning version of HLS you can get.

I have backups of the original HLS code (Source 2007 and Source 2003,)
I haven't backed up the 2003 CS code or the 2007 TFC code, as I haven't touched it much. 
When it starts heavily being touched, then I'll provide backups of the old shit.
I've also provided the GoldSource code to HL and DMC. (multiple versions!)
I am not providing the shared folder to these games, there's not much there, and it didn't keep complex enough to be worth my time uploading.
The source code to Quake Team Fortress can be found, just search up Quake Team Fortress 2.5 source code. It is all in QuakeC.

Also remember, Valve is known for breaking their games with updates. 
Just because it is the newest version, doesn't mean it is is the BEST version.

Remind me to ask Blue24 to extract the name of the code files used to build TFC and CS.

Okay, so...

==== Half-Life ====

Right now the only compilable solution is HLDMS (HL1MP) and is currently what I've been building off of.
In a perfect world we wouldn't need two game folders for these games, 
and since we're porting over and correcting a lot of logic using GoldSource as a reference, 
I don't think the two games being split will be a reality for much longer.

==== Team Fortress Classic ====

TFC shouldn't be TOO far off from compiling... But it was cannibalized and mutilated over the years. 
The code hasn't been touched since 2003, but it was converted to each Source until Source 2007. 
Chunks of the code had also been ripped out of it for TF2, so there's tons of logic missing from it.
Luckily, TFC was just a port of QTF to Half-Life, meaning it shouldn't be too hard to fix up.
Linux compiles of GoldSource games also leave in the files used to compile the game, so that will be useful...

==== Counter-Strike ====

Some how, Counter-Strike 1.6 (at least an in-development version of it) was being ported to Source back in 2003.
Now, out of these solutions, this one is the most errors as Source 2003 was closer to GoldSource than Source.
It's still doable, but it will take a bit of time. However, there's code that wasn't ported over.
My guess is that it didn't convert well to Source or it just straight up didn't get converted,
Half-Life: Source has a few instances of that where code just wasn't ported at all.

==== Deathmatch Classic ====

While we magically got the TFC port in some skeleton state, DMC was never found.
We have all of the materials and VMFs, but not the code. 
At some point people assumed that TFC and DMC didn't have Source Engine ports...
They were clearly wrong as the TFC code surfaced, meaning DMC likely either got so mangled during conversion from 2003 to 2007
or they abanndoned the project early on and it didn't get into any tree that might of been able to convert it.
(and was outside of 2003 code tree that leaked, along side TFC, as it too was missing there.)
I haven't make a project or a solution for this yet,
but just clone the HL1MP stuff and edit it with Notepad++ so it has DMC references and remove the un-needed HLS code.
Also, in the GitHub release of the HL SDK, ThreeWave was snuck in... Would be a shame if it stayed hidden... ;)
(if you don't get what I'm getting at, please provide ThreeWave in DMC for Source.)