
@echo off

REM This file copies the binaries sent to the outter GAME folder to a folder inside this one called "Game". This new Game folder only has the DLL's themselves in a similar path as they were originally: "Game/cstrike/client.dll" for example, same for server.dll. No need to include the entire game assets on github, but we want recently compiled DLL's for convenience.

REM In short, just run this bat before pushing to github to let binaries be downloaded conveniently.



REM !!! IF-THEN:
REM IF EXIST "path/or/something" (
REM     stuff
REM     stuff
REM     stuff
REM ) ELSE (
REM     stuff
REM     stuff
REM     stuff
REM )




REM at a minimum
mkdir "Game"


IF EXIST "../GAME/cstrike/bin" (
    mkdir "Game/cstrike/bin" >nul
    xcopy /F /y "../GAME/cstrike/bin/client.dll" "Game/cstrike/bin"
    xcopy /F /y "../GAME/cstrike/bin/server.dll" "Game/cstrike/bin"
)
IF EXIST "../GAME/hl1mp/bin" (
    mkdir "Game/hl1mp/bin" >nul
    xcopy /F /y "../GAME/hl1mp/bin/client.dll" "Game/hl1mp/bin"
    xcopy /F /y "../GAME/hl1mp/bin/server.dll" "Game/hl1mp/bin"
)
IF EXIST "../GAME/tfc/bin" (
    mkdir "Game/tfc/bin" >nul
    xcopy /F /y "../GAME/tfc/bin/client.dll" "Game/tfc/bin"
    xcopy /F /y "../GAME/tfc/bin/server.dll" "Game/tfc/bin"
)


pause