//========= Copyright � 1996-2005, Valve Corporation, All rights reserved. ============//
//
// Purpose: 
//
// $NoKeywords: $
//
//=============================================================================//
#include	"cbase.h"
#include	"AI_Default.h"
#include	"AI_Task.h"
#include	"AI_Schedule.h"
#include	"AI_Node.h"
#include	"AI_Hull.h"
#include	"AI_Hint.h"
#include	"AI_Route.h"
#include	"soundent.h"
#include	"game.h"
#include	"NPCEvent.h"
#include	"EntityList.h"
#include	"activitylist.h"
#include	"hl1_basegrenade.h"
#include	"animation.h"
#include	"IEffects.h"
#include	"vstdlib/random.h"
#include	"engine/IEngineSound.h"
#include	"ammodef.h"
#include	"soundenvelope.h"
#include	"hl1_ai_basenpc.h"
#include	"ndebugoverlay.h"
#include	"smoke_trail.h"
#include	"beam_shared.h"
#include	"grenade_homer.h"

//#define	 HOMER_TRAIL0_LIFE		0.1
//#define	 HOMER_TRAIL1_LIFE		0.2
//#define	 HOMER_TRAIL2_LIFE		3.0//	1.0

//#define  SF_NOTRANSITION 128

#define SF_WAITFORTRIGGER	(0x04 | 0x40) // UNDONE: Fix!
#define SF_NOWRECKAGE		0x08

// Adding this line will tell the game what health the apache needs when no skill configuration is loaded, default is 0 - Theuaredead
ConVar	sk_apache_health( "sk_apache_health", "0");

//extern short g_sModelIndexFireball;

class CNPC_Apache : public CHL1BaseNPC
{
	DECLARE_CLASS( CNPC_Apache, CHL1BaseNPC );
public:
	DECLARE_DATADESC();

	void Spawn( void );
	void Precache( void );
	Class_T  Classify( void ) { return CLASS_HUMAN_MILITARY; };
	int  BloodColor( void ) { return DONT_BLEED; }
	void Killed( CBaseEntity *pevAttacker, int iGib );
	void GibMonster( void );

	void SetObjectCollisionBox( void )
	{
		//pev->absmin = pev->origin + Vector( -300, -300, -172);
		//pev->absmax = pev->origin + Vector(300, 300, 8);

		Vector vMins = GetAbsOrigin() + Vector( -300, -300, -172);
		Vector vMaxs = GetAbsOrigin() + Vector(300, 300, 8);
		SetCollisionBounds( vMins, vMaxs );
	}

	void HuntThink( void );
	void FlyTouch( CBaseEntity *pOther );
	void CrashTouch( CBaseEntity *pOther );
	void DyingThink( void );
	void StartupUse( CBaseEntity *pActivator, CBaseEntity *pCaller, USE_TYPE useType, float value );
	void NullThink( void );

	void ShowDamage( void );
	void Flight( void );
	void FireRocket( void );
	bool FireGun( void );

	//These arent in GoldSource! - Theuaredead`
	/*
	void AimRocketGun( void );
	void InitializeRotorSound( void );
	void LaunchRocket( Vector &viewDir, int damage, int radius, Vector vecLaunchPoint );
	int  ObjectCaps( void );
	*/

	int m_iRockets;
	float m_flForce;
	float m_flNextRocket;

//	int m_iAmmoType;

	Vector m_vecTarget;
	Vector m_posTarget;

	Vector m_vecDesired;
	Vector m_posDesired;

	Vector m_vecGoal;

	Vector m_angGun;
	float m_flLastSeen;
	float m_flPrevSeen;
	
	int m_iSoundState; // don't save this

	int m_iSpriteTexture;
	int m_iExplode;
	int m_iBodyGibs;
//	int	m_nDebrisModel;

	float m_flGoalSpeed;

	int m_iDoSmokePuff;
	CBeam *m_pBeam;
};

LINK_ENTITY_TO_CLASS ( monster_apache, CNPC_Apache );

BEGIN_DATADESC( CNPC_Apache )
	DEFINE_FIELD( m_iRockets, FIELD_INTEGER ),
	DEFINE_FIELD( m_flForce, FIELD_FLOAT ),
	DEFINE_FIELD( m_flNextRocket, FIELD_TIME ),
	DEFINE_FIELD( m_vecTarget, FIELD_VECTOR ),
	DEFINE_FIELD( m_posTarget, FIELD_POSITION_VECTOR ),
	DEFINE_FIELD( m_vecDesired, FIELD_VECTOR ),
	DEFINE_FIELD( m_posDesired, FIELD_POSITION_VECTOR ),
	DEFINE_FIELD( m_vecGoal, FIELD_VECTOR ),
	DEFINE_FIELD( m_angGun, FIELD_VECTOR ),
	DEFINE_FIELD( m_flLastSeen, FIELD_TIME ),
	DEFINE_FIELD( m_flPrevSeen, FIELD_TIME ),
//	DEFINE_FIELD( m_iSoundState, FIELD_INTEGER ),
//	DEFINE_FIELD( m_iSpriteTexture, FIELD_INTEGER ),
//	DEFINE_FIELD( m_iExplode, FIELD_INTEGER ),
//	DEFINE_FIELD( m_iBodyGibs, FIELD_INTEGER ),
	DEFINE_FIELD( m_pBeam, FIELD_CLASSPTR ),
	DEFINE_FIELD( m_flGoalSpeed, FIELD_FLOAT ),
	DEFINE_FIELD( m_iDoSmokePuff, FIELD_INTEGER ),
END_DATADESC()

void CNPC_Apache::Spawn( void )
{
	Precache( );

	// motor
	SetSolid( SOLID_BBOX );
	AddSolidFlags( FSOLID_NOT_STANDABLE );
	SetMoveType( MOVETYPE_FLY );

	SetModel( "models/apache.mdl" );
	UTIL_SetSize( this, Vector( -32, -32, -32 ), Vector( 32, 32, 32 ) );
	UTIL_SetOrigin( this, GetAbsOrigin() );

	SetRenderColor( 255, 255, 255, 255 );

	AddFlag( FL_NPC );
	TakeDamage( DAMAGE_AIM );
	m_iHealth			= sk_apache_health.GetFloat();

	m_flFieldOfView = -0.707; // 270 degrees

	SetSequence( 0 );
	ResetSequenceInfo( );
	SetCycle( RANDOM_LONG(0, 0xFF) );

	//m_fHelicopterFlags	= BITS_HELICOPTER_MISSILE_ON | BITS_HELICOPTER_GUN_ON;

	InitBoneControllers();

	if (GetSpawnFlags() & SF_WAITFORTRIGGER)
	{
		SetUse( StartupUse );
	}
	else
	{
		SetThink( HuntThink );
		SetTouch( FlyTouch );
		SetNextThink( gpGlobals->curtime + 1.0f );
	}

	m_iRockets = 10;

//	Vector vecSurroundingMins( -300, -300, -172);
//	Vector vecSurroundingMaxs(300, 300, 8);
//	CollisionProp()->SetSurroundingBoundsType( USE_SPECIFIED_BOUNDS, &vecSurroundingMins, &vecSurroundingMaxs );

//	BaseClass::Spawn();
}

void CNPC_Apache::Precache( void )
{
	PrecacheModel( "models/apache.mdl" );

	//rotor1 goes here
	PrecacheScriptSound( "Apache.Rotor" );
	//rotor3 goes here
	//whine1 goes here

	PrecacheScriptSound( "Apache.Die" );

	m_iSpriteTexture = PrecacheModel( "sprites/white.vmt" );

	PrecacheScriptSound( "Apache.FireGun" );

	PrecacheModel( "sprites/lgtning.vmt" );

	m_iExplode	= PrecacheModel( "sprites/fexplo.vmt" );
	m_iBodyGibs = PrecacheModel( "models/metalplategibs_green.mdl" );

	UTIL_PrecacheOther( "hvr_rocket" );

	BaseClass::Precache();
}

void CNPC_Apache::NullThink( void )
{
	StudioFrameAdvance( );
	//pev->nextthink = gpGlobals->time + 0.5;
	SetNextThink( gpGlobals->curtime + 0.5f );
}

void CNPC_Apache::StartupUse( CBaseEntity *pActivator, CBaseEntity *pCaller, USE_TYPE useType, float value )
{
	SetThink( HuntThink );
	SetTouch( FlyTouch );
	//pev->nextthink = gpGlobals->time + 0.1;
	SetNextThink( gpGlobals->curtime + 0.1f );
	SetUse( NULL );
}

void CNPC_Apache::Killed ( entvars_t *pevAttacker, int iGib )
{
	SetMoveType( MOVETYPE_TOSS );
	SetGravity( 0.3 );

	//STOP_SOUND( ENT(pev), CHAN_STATIC, "apache/ap_rotor2.wav" );

	UTIL_SetSize( this, Vector( -32, -32, -64), Vector( 32, 32, 0) );
	SetThink( DyingThink );
	SetTouch( CrashTouch );
	SetNextThink( gpGlobals->curtime + 0.1f );
	m_iHealth = 0;
	TakeDamage( DAMAGE_NO );

	if (pev->spawnflags & SF_NOWRECKAGE)
	{
		m_flNextRocket = gpGlobals->curtime + 4.0f;
	}
	else
	{
		m_flNextRocket = gpGlobals->curtime + 15.0f;
	}
}

void CNPC_Apache::DyingThink( void )
{
	StudioFrameAdvance( );
	SetNextThink( gpGlobals->curtime + 0.1f );

	qAngularVel = qAngularVel * 1.02;

	// still falling?
	if (m_flNextRocket > gpGlobals->curtime )
	{
		//CONVERT 1
		/*
		// random explosions
		MESSAGE_BEGIN( MSG_PVS, SVC_TEMPENTITY, pev->origin );
			WRITE_BYTE( TE_EXPLOSION);		// This just makes a dynamic light now
			WRITE_COORD( pev->origin.x + RANDOM_FLOAT( -150, 150 ));
			WRITE_COORD( pev->origin.y + RANDOM_FLOAT( -150, 150 ));
			WRITE_COORD( pev->origin.z + RANDOM_FLOAT( -150, -50 ));
			WRITE_SHORT( g_sModelIndexFireball );
			WRITE_BYTE( RANDOM_LONG(0,29) + 30  ); // scale * 10
			WRITE_BYTE( 12  ); // framerate
			WRITE_BYTE( TE_EXPLFLAG_NONE );
		MESSAGE_END();

		// lots of smoke
		MESSAGE_BEGIN( MSG_PVS, SVC_TEMPENTITY, pev->origin );
			WRITE_BYTE( TE_SMOKE );
			WRITE_COORD( pev->origin.x + RANDOM_FLOAT( -150, 150 ));
			WRITE_COORD( pev->origin.y + RANDOM_FLOAT( -150, 150 ));
			WRITE_COORD( pev->origin.z + RANDOM_FLOAT( -150, -50 ));
			WRITE_SHORT( g_sModelIndexSmoke );
			WRITE_BYTE( 100 ); // scale * 10
			WRITE_BYTE( 10  ); // framerate
		MESSAGE_END();

		Vector vecSpot = pev->origin + (pev->mins + pev->maxs) * 0.5;
		MESSAGE_BEGIN( MSG_PVS, SVC_TEMPENTITY, vecSpot );
			WRITE_BYTE( TE_BREAKMODEL);

			// position
			WRITE_COORD( vecSpot.x );
			WRITE_COORD( vecSpot.y );
			WRITE_COORD( vecSpot.z );

			// size
			WRITE_COORD( 400 );
			WRITE_COORD( 400 );
			WRITE_COORD( 132 );

			// velocity
			WRITE_COORD( pev->velocity.x ); 
			WRITE_COORD( pev->velocity.y );
			WRITE_COORD( pev->velocity.z );

			// randomization
			WRITE_BYTE( 50 ); 

			// Model
			WRITE_SHORT( m_iBodyGibs );	//model id#

			// # of shards
			WRITE_BYTE( 4 );	// let client decide

			// duration
			WRITE_BYTE( 30 );// 3.0 seconds

			// flags

			WRITE_BYTE( BREAK_METAL );
		MESSAGE_END();
		*/

		// don't stop it we touch a entity
		GetFlags( FL_ONGROUND );
		//pev->flags &= ~FL_ONGROUND;
		SetNextThink( gpGlobals->curtime + 0.2f );
		return;
	}
	else
	{

		//CONVERT 2
	/*
		Vector vecSpot = pev->origin + (pev->mins + pev->maxs) * 0.5;

		// fireball
		MESSAGE_BEGIN( MSG_PVS, SVC_TEMPENTITY, vecSpot );
			WRITE_BYTE( TE_SPRITE );
			WRITE_COORD( vecSpot.x );
			WRITE_COORD( vecSpot.y );
			WRITE_COORD( vecSpot.z + 256 );
			WRITE_SHORT( m_iExplode );
			WRITE_BYTE( 120 ); // scale * 10
			WRITE_BYTE( 255 ); // brightness
		MESSAGE_END();
		
		// big smoke
		MESSAGE_BEGIN( MSG_PVS, SVC_TEMPENTITY, vecSpot );
			WRITE_BYTE( TE_SMOKE );
			WRITE_COORD( vecSpot.x );
			WRITE_COORD( vecSpot.y );
			WRITE_COORD( vecSpot.z + 512 );
			WRITE_SHORT( g_sModelIndexSmoke );
			WRITE_BYTE( 250 ); // scale * 10
			WRITE_BYTE( 5  ); // framerate
		MESSAGE_END();

		// blast circle
		MESSAGE_BEGIN( MSG_PVS, SVC_TEMPENTITY, pev->origin );
			WRITE_BYTE( TE_BEAMCYLINDER );
			WRITE_COORD( pev->origin.x);
			WRITE_COORD( pev->origin.y);
			WRITE_COORD( pev->origin.z);
			WRITE_COORD( pev->origin.x);
			WRITE_COORD( pev->origin.y);
			WRITE_COORD( pev->origin.z + 2000 ); // reach damage radius over .2 seconds
			WRITE_SHORT( m_iSpriteTexture );
			WRITE_BYTE( 0 ); // startframe
			WRITE_BYTE( 0 ); // framerate
			WRITE_BYTE( 4 ); // life
			WRITE_BYTE( 32 );  // width
			WRITE_BYTE( 0 );   // noise
			WRITE_BYTE( 255 );   // r, g, b
			WRITE_BYTE( 255 );   // r, g, b
			WRITE_BYTE( 192 );   // r, g, b
			WRITE_BYTE( 128 ); // brightness
			WRITE_BYTE( 0 );		// speed
		MESSAGE_END();
	*/
		//EMIT_SOUND(ENT(pev), CHAN_STATIC, "weapons/mortarhit.wav", 1.0, 0.3);

		CPASAttenuationFilter filter( this );
		EmitSound( filter, entindex(), "Apache.Die");

		RadiusDamage( GetAbsOrigin(), this, this, 300, CLASS_NONE, DMG_BLAST );

		if (/*!(pev->spawnflags & SF_NOWRECKAGE) && */(GetFlags() & FL_ONGROUND))
		{
			CBaseEntity *pWreckage = Create( "cycler_wreckage", GetAbsOrigin(), GetAbsAngles() );
			// SET_MODEL( ENT(pWreckage->pev), STRING(pev->model) );
			UTIL_SetSize( pWreckage->this, Vector( -200, -200, -128 ), Vector( 200, 200, -32 ) );
			pWreckage->SetCycle() = SetCycle();
			pWreckage->SetSequence() = SetSequence();
			pWreckage->m_flPlaybackRate = 0;
			pWreckage->m_flDmgTime = gpGlobals->curtime + 5f;
		}
		
		// CONVERT 3
		/*
		// gibs
		vecSpot = pev->origin + (pev->mins + pev->maxs) * 0.5;
		MESSAGE_BEGIN( MSG_PVS, SVC_TEMPENTITY, vecSpot );
			WRITE_BYTE( TE_BREAKMODEL);

			// position
			WRITE_COORD( vecSpot.x );
			WRITE_COORD( vecSpot.y );
			WRITE_COORD( vecSpot.z + 64);

			// size
			WRITE_COORD( 400 );
			WRITE_COORD( 400 );
			WRITE_COORD( 128 );

			// velocity
			WRITE_COORD( 0 ); 
			WRITE_COORD( 0 );
			WRITE_COORD( 200 );

			// randomization
			WRITE_BYTE( 30 ); 

			// Model
			WRITE_SHORT( m_iBodyGibs );	//model id#

			// # of shards
			WRITE_BYTE( 200 );

			// duration
			WRITE_BYTE( 200 );// 10.0 seconds

			// flags

			WRITE_BYTE( BREAK_METAL );
		MESSAGE_END();
		*/

		SetThink( SUB_Remove );
		SetNextThink( gpGlobals->curtime + 0.1f );
	}
}

void CNPC_Apache::FlyTouch( CBaseEntity *pOther )
{
	// bounce if we hit something solid
	if ( pOther->GetSolid() == SOLID_BSP) 
	{
		const trace_t &tr = CBaseEntity::GetTouchTrace( );

		// UNDONE, do a real bounce
		ApplyAbsVelocityImpulse( tr.plane.normal * (GetAbsVelocity().Length() + 200) );
	}
}

void CNPC_Apache::CrashTouch( CBaseEntity *pOther )
{
	// only crash if we hit something solid
	if ( pOther->GetSolid() == SOLID_BSP) 
	{
		SetTouch( NULL );
		m_flNextRocket = gpGlobals->curtime;
		SetNextThink( gpGlobals->curtime );
	}
}

void CNPC_Apache::GibMonster( void )
{
	// EMIT_SOUND_DYN(ENT(pev), CHAN_VOICE, "common/bodysplat.wav", 0.75, ATTN_NORM, 0, 200);		
}

void CNPC_Apache::HuntThink( void )
{
	StudioFrameAdvance( );
	SetNextThink( gpGlobals->curtime + 0.1f );

	ShowDamage( );

	if ( GetGoalEnt() == NULL && m_target != NULL_STRING )// this monster has a target
	{
		SetGoalEnt( gEntList.FindEntityByName( NULL, m_target ) );
		if (GetGoalEnt())
		{
			m_vecDesiredPosition = GetGoalEnt()->GetLocalOrigin();

			// FIXME: orienation removed from path_corners!
			AngleVectors( GetGoalEnt()->GetLocalAngles(), &m_vecGoalOrientation );
		}
	}

	// if (m_hEnemy == NULL)
	{
		GetSenses()->Look( 4092 );
		//m_hEnemy = BestVisibleEnemy( );
		ChooseEnemy();
	}

	// generic speed up
	if (m_flGoalSpeed < 800)
		m_flGoalSpeed += 5;

	if( HasEnemy() )
		{
		// ALERT( at_console, "%s\n", STRING( m_hEnemy->pev->classname ) );
		if (FVisible( GetEnemy() ))
			{
				if (m_flLastSeen < gpGlobals->curtime - 5)
				{
					m_flPrevSeen = gpGlobals->curtime;
				}

				m_flLastSeen = gpGlobals->curtime;
				m_vecTargetPosition = GetEnemy()->WorldSpaceCenter();
			}
		}
		else
		{
			HasEnemy();
		}
	}

	//m_vecTarget = (m_posTarget - pev->origin).Normalize();

	Vector targetDir = m_vecTargetPosition - GetAbsOrigin();
	VectorNormalize( targetDir ); 

	//This is half-done... - Theuaredead`
	Vector desiredDir = m_vecDesiredPosition - GetAbsOrigin();
	float flLength = (GetAbsOrign() - m_posDesired).Length();

	VectorNormalize( desiredDir ); 

	if (m_pGoalEnt)
	{
		// ALERT( at_console, "%.0f\n", flLength );

		if (flLength < 128)
		{
			m_pGoalEnt = UTIL_FindEntityByTargetname( NULL, STRING( m_pGoalEnt->pev->target ) );
			if (m_pGoalEnt)
			{
				m_posDesired = m_pGoalEnt->pev->origin;
				UTIL_MakeAimVectors( m_pGoalEnt->pev->angles );
				GetGoalEnt() = gpGlobals->v_forward;
				flLength = (pev->origin - m_posDesired).Length();
			}
		}
	}
	else
	{
		m_vecDesiredPosition = GetAbsOrigin();
	}

	if (flDistToDesiredPosition > 250) // 500
	{
		// float flLength2 = (m_posTarget - pev->origin).Length() * (1.5 - DotProduct((m_posTarget - pev->origin).Normalize(), pev->velocity.Normalize() ));
		// if (flLength2 < flLength)
		if (m_flLastSeen + 90 > gpGlobals->curtime && DotProduct( v1, v2 ) > 0.25)
		{
			m_vecGoalOrientation = ( m_vecTargetPosition - GetAbsOrigin());
		}
		else
		{
			m_vecGoalOrientation = (m_vecDesiredPosition - GetAbsOrigin());
		}
	}
	else
	{
		m_vecGoalOrientation = GetGoalEnt();
	}

	Flight( );

	// ALERT( at_console, "%.0f %.0f %.0f\n", gpGlobals->time, m_flLastSeen, m_flPrevSeen );
	if ((m_flLastSeen + 1 > gpGlobals->time) && (m_flPrevSeen + 2 < gpGlobals->time))
	{
		if (FireGun( ))
		{
			// slow down if we're fireing
			if (m_flGoalSpeed > 400)
				m_flGoalSpeed = 400;
		}

		// don't fire rockets and gun on easy mode
		if (g_iSkillLevel == SKILL_EASY)
			m_flNextRocket = gpGlobals->time + 10.0;
	}

	UTIL_MakeAimVectors( pev->angles );
	Vector vecEst = (gpGlobals->v_forward * 800 + pev->velocity).Normalize( );
	// ALERT( at_console, "%d %d %d %4.2f\n", pev->angles.x < 0, DotProduct( pev->velocity, gpGlobals->v_forward ) > -100, m_flNextRocket < gpGlobals->time, DotProduct( m_vecTarget, vecEst ) );

	if ((m_iRockets % 2) == 1)
	{
		FireRocket( );
		m_flNextRocket = gpGlobals->curtime + 0.5;
		if (m_iRockets <= 0)
		{
			m_flNextRocket = gpGlobals->curtime + 10;
			m_iRockets = 10;
		}
	}
	else if (DotProduct( GetAbsVelocity(), vForward ) > -100 && m_flNextRocket < gpGlobals->curtime)
	{
		if (m_flLastSeen + 60 > gpGlobals->curtime)
		{
			if (GetEnemy() != NULL)
			{
				// make sure it's a good shot
				if (DotProduct( vTargetDir, vecEst) > .965)
				{
					trace_t tr;
					
					UTIL_TraceLine( GetAbsOrigin(), GetAbsOrigin() + vecEst * 4096, MASK_ALL, this, COLLISION_GROUP_NONE, &tr);

			//		NDebugOverlay::Line(GetAbsOrigin(), tr.endpos, 255,0,255, false, 5);

					if ((tr.endpos - m_vecTargetPosition).Length() < 512)
						FireRocket( );
				}
			}
			else
			{
				trace_t tr;
				
				UTIL_TraceLine( GetAbsOrigin(), GetAbsOrigin() + vecEst * 4096, MASK_ALL, this, COLLISION_GROUP_NONE, &tr);
				// just fire when close
				if ((tr.endpos - m_vecTargetPosition).Length() < 512)
					FireRocket( );
			}
		}
	}
}

void CNPC_Apache::Flight( void )
{
	// tilt model 5 degrees
	QAngle angAdj = QAngle( 5.0, 0, 0 );

	// estimate where I'll be facing in one seconds
	Vector forward, right, up;
	AngleVectors( GetAbsAngles() + GetLocalAngularVelocity() * 2 + angAdj, &forward, &right, &up );
	// Vector vecEst1 = pev->origin + pev->velocity + gpGlobals->v_up * m_flForce - Vector( 0, 0, 384 );
	// float flSide = DotProduct( m_posDesired - vecEst1, gpGlobals->v_right );
	
	QAngle angVel = GetLocalAngularVelocity();
	float flSide = DotProduct( m_vecGoalOrientation, right );

	if (flSide < 0)
	{
		if ( angVel.y < 60)
		{
			 angVel.y += 8; // 9 * (3.0/2.0);
		}
	}
	else
	{
		if ( angVel.y > -60)
		{
			 angVel.y -= 8; // 9 * (3.0/2.0);
		}
	}
	angVel.y *= 0.98;
	SetLocalAngularVelocity( angVel );

	Vector vecVel = GetAbsVelocity();

	// estimate where I'll be in two seconds
	AngleVectors( GetAbsAngles() + GetLocalAngularVelocity() * 1 + angAdj, &forward, &right, &up );
	Vector vecEst = GetAbsOrigin() + vecVel * 2.0 + up * m_flForce * 20 - Vector( 0, 0, 384 * 2 );

	// add immediate force
	AngleVectors( GetAbsAngles() + angAdj, &forward, &right, &up );

	vecVel.x += up.x * m_flForce;
	vecVel.y += up.y * m_flForce;
	vecVel.z += up.z * m_flForce;
	// add gravity
	vecVel.z -= 38.4; // 32ft/sec

	float flSpeed = vecVel.Length();
	float flDir = DotProduct( Vector( forward.x, forward.y, 0 ), Vector( vecVel.x, vecVel.y, 0 ) );
	if (flDir < 0)
		flSpeed = -flSpeed;

	float flDist = DotProduct( m_vecDesiredPosition - vecEst, forward );

	// float flSlip = DotProduct( pev->velocity, gpGlobals->v_right );
	float flSlip = -DotProduct( m_vecDesiredPosition - vecEst, right );

	angVel = GetLocalAngularVelocity();
	// fly sideways
	if (flSlip > 0)
	{
		if (GetAbsAngles().z > -30 && angVel.z > -15)
			angVel.z -= 4;
		else
			angVel.z += 2;
	}
	else
	{

		if (GetAbsAngles().z < 30 && angVel.z < 15)
			angVel.z += 4;
		else
			angVel.z -= 2;
	}
	SetLocalAngularVelocity( angVel );

	// sideways drag
	vecVel.x = vecVel.x * (1.0 - fabs( right.x ) * 0.05);
    vecVel.y = vecVel.y * (1.0 - fabs( right.y ) * 0.05);
	vecVel.z = vecVel.z * (1.0 - fabs( right.z ) * 0.05);

	// general drag
	vecVel = vecVel * 0.995;
	
	// apply power to stay correct height
	if (m_flForce < 80 && vecEst.z < m_vecDesiredPosition.z) 
	{
		m_flForce += 12;
	}
	else if (m_flForce > 30)
	{
		if (vecEst.z > m_vecDesiredPosition.z) 
			m_flForce -= 8;
	}

	// pitch forward or back to get to target
	if (flDist > 0 && flSpeed < m_flGoalSpeed && GetAbsAngles().x + angVel.x < 40)
	{
		// ALERT( at_console, "F " );
		// lean forward
		angVel.x += 12.0;
	}
	else if (flDist < 0 && flSpeed > -50 && GetAbsAngles().x + angVel.x  > -20)
	{
		// ALERT( at_console, "B " );
		// lean backward
		angVel.x -= 12.0;
	}
	else if (GetAbsAngles().x + angVel.x < 0)
	{
		// ALERT( at_console, "f " );
		angVel.x += 4.0;
	}
	else if (GetAbsAngles().x + angVel.x > 0)
	{
		// ALERT( at_console, "b " );
		angVel.x -= 4.0;
	}

	// ALERT( at_console, "%.0f %.0f : %.0f %.0f : %.0f %.0f : %.0f\n", pev->origin.x, pev->velocity.x, flDist, flSpeed, pev->angles.x, pev->avelocity.x, m_flForce ); 
	// ALERT( at_console, "%.0f %.0f : %.0f %0.f : %.0f\n", pev->origin.z, pev->velocity.z, vecEst.z, m_posDesired.z, m_flForce ); 

	// make rotor, engine sounds
	if (m_iSoundState == 0)
	{
		//EMIT_SOUND_DYN(ENT(pev), CHAN_STATIC, "apache/ap_rotor2.wav", 1.0, 0.3, 0, 110 );

		CPASAttenuationFilter filter( this );
		EmitSound( filter, entindex(), "Apache.Rotor");

		// EMIT_SOUND_DYN(ENT(pev), CHAN_STATIC, "apache/ap_whine1.wav", 0.5, 0.2, 0, 110 );

		m_iSoundState = SND_CHANGE_PITCH; // hack for going through level transitions
	}
	else
	{
		CBaseEntity *pPlayer = NULL;

		pPlayer = UTIL_FindEntityByClassname( NULL, "player" );
		// UNDONE: this needs to send different sounds to every player for multiplayer.	
		if (pPlayer)
		{

			float pitch = DotProduct( pev->velocity - pPlayer->pev->velocity, (pPlayer->pev->origin - pev->origin).Normalize() );

			pitch = (int)(100 + pitch / 50.0);

			if (pitch > 250) 
				pitch = 250;
			if (pitch < 50)
				pitch = 50;
			if (pitch == 100)
				pitch = 101;

			float flVol = (m_flForce / 100.0) + .1;
			if (flVol > 1.0) 
				flVol = 1.0;

			//EMIT_SOUND_DYN(ENT(pev), CHAN_STATIC, "apache/ap_rotor2.wav", 1.0, 0.3, SND_CHANGE_PITCH | SND_CHANGE_VOL, pitch);
			CPASAttenuationFilter filter( this );
			EmitSound( filter, entindex(), "Apache.Rotor");

		}
		// EMIT_SOUND_DYN(ENT(pev), CHAN_STATIC, "apache/ap_whine1.wav", flVol, 0.2, SND_CHANGE_PITCH | SND_CHANGE_VOL, pitch);
	
		// ALERT( at_console, "%.0f %.2f\n", pitch, flVol );
	}
}

void CNPC_Apache::FireRocket( void )
{
	static float side = 1.0;
	static int count;
	Vector vForward, vRight, vUp;


	AngleVectors( GetAbsAngles(), &vForward, &vRight, &vUp );
	Vector vecSrc = GetAbsOrigin() + 1.5 * ( vForward * 21 + vRight * 70 * side + vUp * -79 );

	// pick firing pod to launch from
	switch( m_iRockets % 5)
	{
	case 0:	vecSrc = vecSrc + vRight * 10; break;
	case 1: vecSrc = vecSrc - vRight * 10; break;
	case 2: vecSrc = vecSrc + vUp * 10; break;
	case 3: vecSrc = vecSrc - vUp * 10; break;
	case 4: break;
	}

	//CONVERT ME!!!
	/*
	MESSAGE_BEGIN( MSG_PVS, SVC_TEMPENTITY, vecSrc );
		WRITE_BYTE( TE_SMOKE );
		WRITE_COORD( vecSrc.x );
		WRITE_COORD( vecSrc.y );
		WRITE_COORD( vecSrc.z );
		WRITE_SHORT( g_sModelIndexSmoke );
		WRITE_BYTE( 20 ); // scale * 10
		WRITE_BYTE( 12 ); // framerate
	MESSAGE_END();
	*/

	CBaseEntity *pRocket = CBaseEntity::Create( "hvr_rocket", vecSrc, pev->angles, edict() );
	if (pRocket)
		pRocket->pev->velocity = pev->velocity + gpGlobals->v_forward * 100;

	m_iRockets--;

	side = - side;
}

bool CNPC_Apache::FireGun( )
{
	Vector vForward, vRight, vUp;

	AngleVectors( GetAbsAngles(), &vForward, &vUp, &vRight );
		
	Vector posGun, angGun;
	GetAttachment( 1, posGun, angGun );

	Vector vecTarget = (m_vecTargetPosition - posGun);

	VectorNormalize( vecTarget );

	Vector vecOut;

	vecOut.x = DotProduct( vForward, vecTarget );
	vecOut.y = -DotProduct( vUp, vecTarget );
	vecOut.z = DotProduct( vRight, vecTarget );

	QAngle angles;

	VectorAngles( vecOut, angles );

	angles.x = -angles.x;
	if (angles.y > 180)
		angles.y = angles.y - 360;
	if (angles.y < -180)
		angles.y = angles.y + 360;
	if (angles.x > 180)
		angles.x = angles.x - 360;
	if (angles.x < -180)
		angles.x = angles.x + 360;

	if (angles.x > m_angGun.x)
		m_angGun.x = min( angles.x, m_angGun.x + 12 );
	if (angles.x < m_angGun.x)
		m_angGun.x = max( angles.x, m_angGun.x - 12 );
	if (angles.y > m_angGun.y)
		m_angGun.y = min( angles.y, m_angGun.y + 12 );
	if (angles.y < m_angGun.y)
		m_angGun.y = max( angles.y, m_angGun.y - 12 );

	m_angGun.y = SetBoneController( 0, m_angGun.y );
	m_angGun.x = SetBoneController( 1, m_angGun.x );

	Vector posBarrel, angBarrel;
	GetAttachment( 0, posBarrel, angBarrel );
	Vector vecGun = (posBarrel - posGun).Normalize( );

	if (DotProduct( vecGun, vecTarget ) > 0.98)
	{
#if 1
		FireBullets( 1, posGun, vecGun, VECTOR_CONE_4DEGREES, 8192, BULLET_MONSTER_12MM, 1 );
		CPASAttenuationFilter filter( this, 0.3f );

		EmitSound( filter, entindex(), "Apache.FireGun" );
#else
		static float flNext;
		TraceResult tr;
		UTIL_TraceLine( posGun, posGun + vecGun * 8192, dont_ignore_monsters, ENT( pev ), &tr );

		if (!m_pBeam)
		{
			m_pBeam = CBeam::BeamCreate( "sprites/lgtning.vmt", 80 );
			m_pBeam->PointEntInit( pev->origin, entindex( ) );
			m_pBeam->SetEndAttachment( 1 );
			m_pBeam->SetColor( 255, 180, 96 );
			m_pBeam->SetBrightness( 192 );
		}

		if (flNext < gpGlobals->time)
		{
			flNext = gpGlobals->time + 0.5;
			m_pBeam->SetStartPos( tr.vecEndPos );
		}
#endif
		return TRUE;
	}
	else
	{
		if (m_pBeam)
		{
			UTIL_Remove( m_pBeam );
			m_pBeam = NULL;
		}
	}
	return FALSE;
}

void CNPC_Apache::ShowDamage( void )
{
	if (m_iDoSmokePuff > 0 || RANDOM_LONG(0,99) > pev->health)
	{
		//CONVERT ME!!!
		/*
		MESSAGE_BEGIN( MSG_PVS, SVC_TEMPENTITY, pev->origin );
			WRITE_BYTE( TE_SMOKE );
			WRITE_COORD( pev->origin.x );
			WRITE_COORD( pev->origin.y );
			WRITE_COORD( pev->origin.z - 32 );
			WRITE_SHORT( g_sModelIndexSmoke );
			WRITE_BYTE( RANDOM_LONG(0,9) + 20 ); // scale * 10
			WRITE_BYTE( 12 ); // framerate
		MESSAGE_END();
		*/
	}
	if (m_iDoSmokePuff > 0)
		m_iDoSmokePuff--;
}

void CNPC_Apache::TakeDamage( const CTakeDamageInfo &inputInfo )
{
	if (pevInflictor->owner == edict())
		return 0;

	if (bitsDamageType & DMG_BLAST)
	{
		flDamage *= 2;
	}

	/*
	if ( (bitsDamageType & DMG_BULLET) && flDamage > 50)
	{
		// clip bullet damage at 50
		flDamage = 50;
	}
	*/

	// ALERT( at_console, "%.0f\n", flDamage );
	return CBaseEntity::TakeDamage( const CTakeDamageInfo &inputInfo );
}

void CNPC_Apache::TraceAttack( entvars_t *pevAttacker, float flDamage, Vector vecDir, TraceResult *ptr, int bitsDamageType)
{
	// ALERT( at_console, "%d %.0f\n", ptr->iHitgroup, flDamage );

	// ignore blades
	if (ptr->iHitgroup == 6 && (bitsDamageType & (DMG_ENERGYBEAM|DMG_BULLET|DMG_CLUB)))
		return;

	// hit hard, hits cockpit, hits engines
	if (flDamage > 50 || ptr->iHitgroup == 1 || ptr->iHitgroup == 2)
	{
		// ALERT( at_console, "%.0f\n", flDamage );
		AddMultiDamage( pevAttacker, this, flDamage, bitsDamageType );
		m_iDoSmokePuff = 3 + (flDamage / 5.0);
	}
	else
	{
		// do half damage in the body
		// AddMultiDamage( pevAttacker, this, flDamage / 2.0, bitsDamageType );
		UTIL_Ricochet( ptr->vecEndPos, 2.0 );
	}
}

class CApacheHVR : public CHL1BaseGrenade
{
DECLARE_CLASS( CApacheHVR, CHL1BaseGrenade );
public:
	DECLARE_DATADESC();

	void Spawn( void );
	void Precache( void );
	void IgniteThink( void );
	void AccelerateThink( void );

	int		Save( CSave &save );
	int		Restore( CRestore &restore );
	static	TYPEDESCRIPTION m_SaveData[];

	int m_iTrail;
	Vector m_vecForward;
};
LINK_ENTITY_TO_CLASS( hvr_rocket, CApacheHVR );

BEGIN_DATADESC( CApacheHVR )
//	DEFINE_FIELD( CApacheHVR, m_iTrail, FIELD_INTEGER ),	// Dont' save, precache
	DEFINE_FIELD( m_vecForward, FIELD_VECTOR ),
END_DATADESC()

//IMPLEMENT_SAVERESTORE( CApacheHVR, CGrenade );

void CApacheHVR :: Spawn( void )
{
	Precache( );
	// motor
	SetMoveType( MOVETYPE_FLY );
	SetSolid( SOLID_BBOX );
	AddSolidFlags( FSOLID_NOT_STANDABLE );

	SetModel(ENT(pev), "models/HVR.mdl");
	UTIL_SetSize(this, Vector( 0, 0, 0), Vector(0, 0, 0));
	UTIL_SetOrigin( this, GetAbsOrigin() );

	SetThink( IgniteThink );
	SetTouch( ExplodeTouch );

	UTIL_MakeAimVectors( GetAbsAngles() );
	m_vecForward = gpGlobals->vForward;
	SetGravity( 0.5 );

	SetNextThink( gpGlobals->curtime + 1.0f );

	m_flDamage = 150;
}


void CApacheHVR::Precache( void )
{
	PrecacheModel("models/HVR.mdl");
	m_iTrail = PrecacheModel("sprites/smoke.vmt");
	PrecacheScriptSound( "ApacheHVR.Ignite );
}


void CApacheHVR::IgniteThink( void  )
{
	// pev->movetype = MOVETYPE_TOSS;

	// pev->movetype = MOVETYPE_FLY;
	pev->effects |= EF_LIGHT;

	// make rocket sound
	EMIT_SOUND( ENT(pev), CHAN_VOICE, "weapons/rocket1.wav", 1, 0.5 );

	// rocket trail
	// CONVERT ME!!!
	/*
	MESSAGE_BEGIN( MSG_BROADCAST, SVC_TEMPENTITY );

		WRITE_BYTE( TE_BEAMFOLLOW );
		WRITE_SHORT(entindex());	// entity
		WRITE_SHORT(m_iTrail );	// model
		WRITE_BYTE( 15 ); // life
		WRITE_BYTE( 5 );  // width
		WRITE_BYTE( 224 );   // r, g, b
		WRITE_BYTE( 224 );   // r, g, b
		WRITE_BYTE( 255 );   // r, g, b
		WRITE_BYTE( 255 );	// brightness

	MESSAGE_END();  // move PHS/PVS data sending into here (SEND_ALL, SEND_PVS, SEND_PHS)
	*/

	// set to accelerate
	SetThink( AccelerateThink );
	pev->nextthink = gpGlobals->time + 0.1;
}


void CApacheHVR::AccelerateThink( void  )
{
	// check world boundaries
	if (pev->origin.x < -4096 || pev->origin.x > 4096 || pev->origin.y < -4096 || pev->origin.y > 4096 || pev->origin.z < -4096 || pev->origin.z > 4096)
	{
		UTIL_Remove( this );
		return;
	}

	// accelerate
	float flSpeed = pev->velocity.Length();
	if (flSpeed < 1800)
	{
		pev->velocity = pev->velocity + m_vecForward * 200;
	}

	// re-aim
	pev->angles = UTIL_VecToAngles( pev->velocity );

	pev->nextthink = gpGlobals->time + 0.1;
}
