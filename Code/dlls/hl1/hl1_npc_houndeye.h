//========= Copyright � 1996-2005, Valve Corporation, All rights reserved. ============//
//
// Purpose: 
//
// $NoKeywords: $
//
//=============================================================================//
#ifndef NPC_HOUNDEYE_H
#define NPC_HOUNDEYE_H
#pragma once

#include "hl1_ai_basenpc.h"

class CNPC_Houndeye : public CHL1BaseNPC
{
	DECLARE_CLASS( CNPC_Houndeye, CHL1BaseNPC );
	
public:
	void Spawn( void );
	void Precache( void );
	Class_T	Classify ( void );
	void HandleAnimEvent( animevent_t *pEvent );
	float MaxYawSpeed  ( void );
	void WarmUpSound ( void );
	void AlertSound( void );
	void DeathSound( const CTakeDamageInfo &info );
	void WarnSound( void );
	void PainSound( const CTakeDamageInfo &info );
	void IdleSound( void );
	void StartTask( const Task_t *pTask );
	void RunTask ( const Task_t *pTask );
	void SonicAttack ( void );
	void PrescheduleThink ( void );
	void SetActivity ( Activity NewActivity );
	Vector WriteBeamColor( void );
	int RangeAttack1Conditions ( float flDot, float flDist );
	bool FValidateHintType ( CAI_Hint *pHint );
	bool ShouldGoToIdleState( void );
	int TranslateSchedule( int scheduleType );
	int SelectSchedule( void );
		
	void Event_Killed( const CTakeDamageInfo &info );
	float FLSoundVolume( CSound *pSound );

	DEFINE_CUSTOM_AI;
	DECLARE_DATADESC();

private:

	int m_iSpriteTexture;
	bool m_fAsleep;// some houndeyes sleep in idle mode if this is set, the houndeye is lying down
	bool m_fDontBlink;// don't try to open/close eye if this bit is set!
	Vector	m_vecPackCenter; // the center of the pack. The leader maintains this by averaging the origins of all pack members.
};

#endif // NPC_HOUNDEYE_H