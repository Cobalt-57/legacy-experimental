//========= Copyright � 1996-2005, Valve Corporation, All rights reserved. ============//
//
// Purpose: 
//
// $NoKeywords: $
//
//=============================================================================//
#ifndef NPC_HEADCRAB_H
#define NPC_HEADCRAB_H
#pragma once

#include "hl1_ai_basenpc.h"

ConVar	sk_headcrab_health( "sk_headcrab_health","20");
ConVar	sk_headcrab_dmg_bite( "sk_headcrab_dmg_bite","10");

class CNPC_Headcrab : public CHL1BaseNPC
{
	DECLARE_CLASS( CNPC_Headcrab, CHL1BaseNPC );
public:
	void Spawn( void );
	void Precache( void );
	void RunTask ( const Task_t *pTask );
	void StartTask ( const Task_t *pTask );
	float MaxYawSpeed( void );
	void LeapTouch ( CBaseEntity *pOther );
	void TouchDamage( CBaseEntity *pOther );
	Vector	Center( void );
	Vector	BodyTarget( const Vector &posSrc );
	// had "bool bNoisy = true" - Theuaredead`
	void IdleSound(void);
	void AlertSound(void);
	void PainSound(const CTakeDamageInfo &info);
	void AttackSound( void );
	void DeathSound(const CTakeDamageInfo &info);
	void BiteSound( void );
	void PrescheduleThink( void );
	Class_T Classify( void );
	void HandleAnimEvent( animevent_t *pEvent );
	int RangeAttack1Conditions ( float flDot, float flDist );
	int RangeAttack2Conditions ( float flDot, float flDist );
	int OnTakeDamage_Alive( const CTakeDamageInfo &inputInfo );
	
	//These don't exist anymore in this code - Theuaredead`
	//int	 SelectSchedule( void );
	//void Touch( CBaseEntity *pOther );
	
	float GetDamageAmount( void ) { return sk_headcrab_dmg_bite.GetFloat(); }
	virtual int		GetVoicePitch( void ) { return 100; }
	virtual float	GetSoundVolume( void ) { return 1.0; }
	int TranslateSchedule( int scheduleType );
	
	int	m_nGibCount;

	DEFINE_CUSTOM_AI;
	DECLARE_DATADESC();

protected:
	Vector	m_vecJumpVel;
};

#endif //NPC_HEADCRAB_H