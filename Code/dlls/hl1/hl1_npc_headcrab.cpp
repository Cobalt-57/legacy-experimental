//========= Copyright � 1996-2005, Valve Corporation, All rights reserved. ============//
//
// Purpose:
//
// $NoKeywords: $
//=============================================================================//

//=========================================================
// headcrab.cpp - tiny, jumpy alien parasite
//=========================================================

#include "cbase.h"
#include "game.h"
#include "AI_Default.h"
#include "AI_Schedule.h"
#include "AI_Hull.h"
#include "AI_Route.h"
#include "AI_Motor.h"
#include "NPCEvent.h"
#include "hl1_npc_headcrab.h"
#include "gib.h"
//#include "AI_Interactions.h"
#include "ndebugoverlay.h"
#include "vstdlib/random.h"
#include "engine/IEngineSound.h"
#include "movevars_shared.h"
#include "soundemittersystem/isoundemittersystembase.h"

extern void ClearMultiDamage(void);
extern void ApplyMultiDamage( void );

//=========================================================
// Monster's Anim Events Go Here
//=========================================================

#define HEADCRAB_GUTS_GIB_COUNT		1
#define HEADCRAB_LEGS_GIB_COUNT		3
#define HEADCRAB_ALL_GIB_COUNT		5

#define HEADCRAB_MAX_JUMP_DIST		256

#define HEADCRAB_RUNMODE_ACCELERATE		1
#define HEADCRAB_RUNMODE_IDLE			2
#define HEADCRAB_RUNMODE_DECELERATE		3
#define HEADCRAB_RUNMODE_FULLSPEED		4
#define HEADCRAB_RUNMODE_PAUSE			5

#define HEADCRAB_RUN_MINSPEED	0.5
#define HEADCRAB_RUN_MAXSPEED	1.0

#define	HC_AE_JUMPATTACK		( 2 )

enum
{
	SCHED_HEADCRAB_RANGE_ATTACK1 = LAST_SHARED_SCHEDULE,
	SCHED_FAST_HEADCRAB_RANGE_ATTACK1,
};

AI_BEGIN_CUSTOM_NPC( monster_headcrab, CNPC_Headcrab )

	//=========================================================
	// > SCHED_HEADCRAB_RANGE_ATTACK1
	//=========================================================
	DEFINE_SCHEDULE
	(
		SCHED_HEADCRAB_RANGE_ATTACK1,
	
		"	Tasks"
		"		TASK_STOP_MOVING			0"
		"		TASK_FACE_IDEAL				0"
		"		TASK_RANGE_ATTACK1			0"
		"		TASK_SET_ACTIVITY			ACTIVITY:ACT_IDLE"
		"		TASK_FACE_IDEAL				0"
		"		TASK_WAIT_RANDOM			0.5"
		"	"
		"	Interrupts"
		"		COND_ENEMY_OCCLUDED"
		"		COND_NO_PRIMARY_AMMO"
	)
	
	//=========================================================
	// > SCHED_FAST_HEADCRAB_RANGE_ATTACK1
	//=========================================================
	DEFINE_SCHEDULE
	(
		SCHED_FAST_HEADCRAB_RANGE_ATTACK1,
	
		"	Tasks"
		"		TASK_STOP_MOVING			0"
		"		TASK_FACE_IDEAL				0"
		"		TASK_RANGE_ATTACK1			0"
		"		TASK_SET_ACTIVITY			ACTIVITY:ACT_IDLE"
		"	"
		"	Interrupts"
		"		COND_ENEMY_OCCLUDED"
		"		COND_NO_PRIMARY_AMMO"
	)

AI_END_CUSTOM_NPC()


//New to Source, is it needed? - Theuaredead`
BEGIN_DATADESC( CNPC_Headcrab )
	// m_nGibCount - don't save

	// Function Pointers
	DEFINE_ENTITYFUNC( LeapTouch ),
	DEFINE_FIELD( m_vecJumpVel, FIELD_VECTOR ),

END_DATADESC()

LINK_ENTITY_TO_CLASS( monster_headcrab, CNPC_Headcrab );


//=========================================================
// Classify - indicates this monster's place in the 
// relationship table.
//=========================================================
Class_T	CNPC_Headcrab::Classify( void )
{
	return CLASS_ALIEN_PREY; 
}

//=========================================================
// Center - returns the real center of the headcrab.  The 
// bounding box is much larger than the actual creature so 
// this is needed for targeting
//=========================================================
Vector CNPC_Headcrab::Center( void )
{
	return Vector( GetAbsOrigin().x, GetAbsOrigin().y, GetAbsOrigin().z + 6 );
}


Vector CNPC_Headcrab::BodyTarget( const Vector &posSrc ) 
{ 
	return( Center() );
}

//=========================================================
// MaxYawSpeed - allows each sequence to have a different
// turn rate associated with it.
//=========================================================

// SetYawSpeed became MaxYawSpeed in Source! - Theuaredead`

//NOTE: The left and right turns were 15 in Source, but 60 in GoldSource.
//		60 devided by 15 is 4.
//		In normal cases, we would devide with this value, 
//		But it appears that the Headcrabs in GoldSource are 10 times slower than Source, not 4 times.
//		This is at least how I understand how this works, but I could be wrong! - Theuaredead`

float CNPC_Headcrab::MaxYawSpeed ( void )
{
	int flYS;

	flYS = 5 / 10;

	switch ( GetActivity() )
	{
	case ACT_IDLE:			
		flYS = 30 / 10;
		break;
	case ACT_RUN:			
	case ACT_WALK:			
		flYS = 20 / 10;
		break;
	case ACT_TURN_LEFT:
	case ACT_TURN_RIGHT:
		flYS = 60 / 10;
		break;
	case ACT_RANGE_ATTACK1:	
		flYS = 30 / 10;
		break;
	default:
		flYS = 30 / 10;
		break;
	}

	//Also, I'm not entirely too certain on what the fuck this value is even supposed to yield? - Theuaredead`
	return flYS;

}

//=========================================================
// HandleAnimEvent - catches the monster-specific messages
// that occur when tagged animation frames are played.
//=========================================================
void CNPC_Headcrab::HandleAnimEvent( animevent_t *pEvent )
{
	switch ( pEvent->event )
	{
		case HC_AE_JUMPATTACK:
		{
			SetGroundEntity( NULL );

			// Take him off ground so engine doesn't instantly reset FL_ONGROUND.
			UTIL_SetOrigin( this, GetAbsOrigin() + Vector( 0 , 0 , 1 ));

			Vector vecJumpDir;
			CBaseEntity *pEnemy = GetEnemy();
			if ( pEnemy )
			{
				Vector vecEnemyEyePos = pEnemy->EyePosition();

				float gravity = sv_gravity.GetFloat();
				if ( gravity <= 1 )
				{
					gravity = 1;
				}

				// How fast does the headcrab need to travel to reach my enemy's eyes given gravity?
				float height = ( vecEnemyEyePos.z - GetAbsOrigin().z );
				if ( height < 16 )
				{
					height = 16;
				}
				else if ( height > 120 )
				{
					height = 120;
				}
				float speed = sqrt( 2 * gravity * height );
				float time = speed / gravity;

				// Scale the sideways velocity to get there at the right time
				vecJumpDir = vecEnemyEyePos - GetAbsOrigin();
				vecJumpDir = vecJumpDir / time;

				// Speed to offset gravity at the desired height.
				vecJumpDir.z = speed;

				// Don't jump too far/fast.
				float distance = vecJumpDir.Length();
				if ( distance > 650 )
				{
					vecJumpDir = vecJumpDir * ( 650.0 / distance );
				}
			}
			else
			{
				// Jump hop, don't care where.
				Vector forward, up;
				AngleVectors( GetAbsAngles(), &forward, NULL, &up );
				vecJumpDir = Vector( forward.x, forward.y, up.z ) * 350;
			}

			int iSound = random->RandomInt( 0 , 2 );
			if ( iSound != 0 )
			{
				AttackSound();
			}

			SetAbsVelocity( vecJumpDir );
			m_flNextAttack = gpGlobals->curtime + 2;
			break;
		}

		default:
		{
			CAI_BaseNPC::HandleAnimEvent( pEvent );
			break;
		}
	}
}

//=========================================================
// Spawn
//=========================================================
void CNPC_Headcrab::Spawn( void )
{
	Precache();

	//New to Source, is it needed? - Theuaredead`
	SetRenderColor( 255, 255, 255, 255 );

	SetModel( "models/headcrab.mdl" );
	UTIL_SetSize(this, Vector(-12, -12, 0), Vector(12, 12, 24));

	//Source stuff, Badis thought it was related to SetSize, but it doesnt seem to be the case? - Theuaredead`
	SetHullType(HULL_TINY);
	SetHullSizeNormal();

	SetSolid( SOLID_BBOX );
	//AddSolidFlags is new to Source, sounds needed. - Theuaredead
	AddSolidFlags( FSOLID_NOT_STANDABLE );
	SetMoveType( MOVETYPE_STEP );
	m_bloodColor		= BLOOD_COLOR_GREEN;
	ClearEffects();
	m_iHealth = sk_headcrab_health.GetFloat();
	//This is how GoldSource defines this. - Theuaredead`
	SetViewOffset( Vector(0, 0, 20) );
	//SetViewOffset( Vector(6, 0, 11) );		// Position of the eyes relative to NPC's origin.

	m_flFieldOfView		= 0.5;
	m_NPCState			= NPC_STATE_NONE;
	//From GoldSource - Theuaredead`
	//When this gets defined in a spawn, please do the math! - Theuaredead`
	//Someone needs to find a fucking way to get this working with the MaxYawSpeed. - Theuaredead`
	//flYS = 5;//!!! should we put this in the monster's changeanim function since turn rates may vary with state/anim?

	//This is not in the GoldSource logic. - Theuaredead`
	m_nGibCount			= HEADCRAB_ALL_GIB_COUNT;
	
	//New to Source, probably needed? - Theuaredead`
	CapabilitiesClear();
	CapabilitiesAdd( bits_CAP_MOVE_GROUND | bits_CAP_INNATE_RANGE_ATTACK1 );

	NPCInit();
}

//=========================================================
// Precache - precaches all resources this monster needs
//=========================================================
void CNPC_Headcrab::Precache( void )
{
	PrecacheModel( "models/headcrab.mdl" );

	//Copied from the HLSU Codebase - Theuaredead`
	//Added missing sounds - Theuaredead`
	PrecacheScriptSound( "Headcrab.Idle" );
	PrecacheScriptSound( "Headcrab.Alert" );
	PrecacheScriptSound( "Headcrab.Pain" );
	PrecacheScriptSound( "Headcrab.Attack" );
	PrecacheScriptSound( "Headcrab.Die" );
	PrecacheScriptSound( "Headcrab.Bite" );

	//This isn't here in GoldSource, not going to rip it out and risk breaking stuff, precaching is important. - Theuaredead`
	BaseClass::Precache();
}

//=========================================================
// RunTask 
//=========================================================
void CNPC_Headcrab::RunTask( const Task_t *pTask )
{
	switch ( pTask->iTask )
	{
		case TASK_RANGE_ATTACK1:
		case TASK_RANGE_ATTACK2:
		{
			if ( IsSequenceFinished() )
			{
				TaskComplete();
				SetTouch( NULL );
				SetIdealActivity( ACT_IDLE );
			}
			break;
		}

		default:
		{
			CAI_BaseNPC::RunTask( pTask );
		}
	}
}

//=========================================================
// LeapTouch - this is the headcrab's touch function when it
// is in the air
//=========================================================
void CNPC_Headcrab::LeapTouch( CBaseEntity *pOther )
{
	// Half-Life Source code - Theuaredead`
	if ( pOther->Classify() == Classify() )
	{
		return;
	}

	// Don't hit if back on ground
	if ( !(GetFlags() & FL_ONGROUND) && ( pOther->IsNPC() || pOther->IsPlayer() ) )
	{
		BiteSound();
		TouchDamage( pOther );
	}

	SetTouch( NULL );

	//Unsure on how to go about converting this, but this is the original function from GoldSource - Theuaredead`
	/*
	if ( !pOther->pev->takedamage )
	{
		return;
	}

	if ( pOther->Classify() == Classify() )
	{
		return;
	}

	// Don't hit if back on ground
	if ( !FBitSet( pev->flags, FL_ONGROUND ) )
	{
		BiteSound();
		
		pOther->TakeDamage( pev, pev, GetDamageAmount(), DMG_SLASH );
	}

	SetTouch( NULL );
	*/
}

//-----------------------------------------------------------------------------
// Purpose: Deal the damage from the headcrab's touch attack.
//-----------------------------------------------------------------------------
void CNPC_Headcrab::TouchDamage( CBaseEntity *pOther )
{
	CTakeDamageInfo info( this, this, GetDamageAmount(), DMG_SLASH );
	//CalculateMeleeDamageForce( &info, GetAbsVelocity(), GetAbsOrigin() );
	pOther->TakeDamage( info );
}

//=========================================================
// PrescheduleThink
//=========================================================
void CNPC_Headcrab::PrescheduleThink( void )
{
	//This line wasn't here in GoldSource? - Theuaredead`
	BaseClass::PrescheduleThink();
	
	// Make the crab coo a little bit in combat state.
	if (( m_NPCState == NPC_STATE_COMBAT ) && ( random->RandomFloat( 0, 5 ) < 0.1 ))
	{
		IdleSound();
	}
}

void CNPC_Headcrab::StartTask( const Task_t *pTask )
{
	//Original from Source - Theuaredead`
/*
	switch ( pTask->iTask )
	{
		case TASK_RANGE_ATTACK1:
		{
			SetIdealActivity( ACT_RANGE_ATTACK1 );
			SetTouch( &CNPC_Headcrab::LeapTouch );
			break;
		}
		default:
		{
			BaseClass::StartTask( pTask );
		}
	}
*/

	//From GoldSource
	//SetTaskStatus ( TASKSTATUS_RUN_MOVE );
	TaskIsRunning();

	switch ( pTask->iTask )
	{
	case TASK_RANGE_ATTACK1:
		{
			//Old GoldSource Way of doing this.
			//EMIT_SOUND_DYN( edict(), CHAN_WEAPON, pAttackSounds[0], GetSoundVolue(), ATTN_IDLE, 0, GetVoicePitch() );
			AttackSound();
			SetIdealActivity( ACT_RANGE_ATTACK1 );
			SetTouch( &CNPC_Headcrab::LeapTouch );
			break;
		}
	default:
		{
			BaseClass::StartTask( pTask );
		}
	}
}

//=========================================================
// RangeAttack1Conditions - used to be CheckRangeAttack1
//=========================================================
int CNPC_Headcrab::RangeAttack1Conditions ( float flDot, float flDist )
{
	//Original from Source - Theuaredead`
	if ( gpGlobals->curtime < m_flNextAttack )
	{
		return( 0 );
	}

	if ( !(GetFlags() & FL_ONGROUND) )
	{
		return( 0 );
	}

	if ( flDist > 256 )
	{
		return( COND_TOO_FAR_TO_ATTACK );
	}
	else if ( flDot < 0.65 )
	{
		return( COND_NOT_FACING_ATTACK );
	}

	return( COND_CAN_RANGE_ATTACK1 );

/*
	//GoldSource - Theuaredead`
	if ( FBitSet( pev->flags, FL_ONGROUND ) && flDist <= 256 && flDot >= 0.65 )
	{
		return TRUE;
	}
	return FALSE;
*/
}

//=========================================================
// RangeAttack2Conditions - used to be CheckRangeAttack2 - Theuaredead`
//=========================================================
int CNPC_Headcrab::RangeAttack2Conditions ( float flDot, float flDist )
{
	return FALSE;
	// BUGBUG: Why is this code here?  There is no ACT_RANGE_ATTACK2 animation.  I've disabled it for now.
	// Keeping this in just for the sake of consistency - Theuaredead
/*
	if ( FBitSet( pev->flags, FL_ONGROUND ) && flDist > 64 && flDist <= 256 && flDot >= 0.5 )
	{
		return TRUE;
	}
	return FALSE;
*/
}

//OnTakeDamage_Alive is TakeDamage - Theuaredead`

int CNPC_Headcrab::OnTakeDamage_Alive( const CTakeDamageInfo &inputInfo )
{
	CTakeDamageInfo info = inputInfo;

	// Don't take any acid damage -- BigMomma's mortar is acid
	if ( info.GetDamageType() & DMG_ACID )
	{
		return 0;
	}

	return BaseClass::OnTakeDamage_Alive( info );
}

//GoldSource original - Theuaredead`
/*
int CHeadCrab :: TakeDamage( entvars_t *pevInflictor, entvars_t *pevAttacker, float flDamage, int bitsDamageType )
{
	// Don't take any acid damage -- BigMomma's mortar is acid
	if ( bitsDamageType & DMG_ACID )
		flDamage = 0;

	return CBaseMonster::TakeDamage( pevInflictor, pevAttacker, flDamage, bitsDamageType );
}
*/

//Copied this over from the HLSU codebase - Theuaredead`

//=========================================================
// IdleSound 
//=========================================================
#define CRAB_ATTN_IDLE				(float)1.5
void CNPC_Headcrab::IdleSound( void )
{
	CPASAttenuationFilter filter(this, ATTN_IDLE );

	CSoundParameters params;
	if ( GetParametersForSound( "Headcrab.Idle", params, NULL ) )
	{
		EmitSound_t ep( params );

		ep.m_flVolume = GetSoundVolume();
		ep.m_nPitch = GetVoicePitch();

		EmitSound( filter, entindex(), ep );
	}
}

//=========================================================
// AlertSound
//=========================================================
void CNPC_Headcrab::AlertSound(void)
{
	CPASAttenuationFilter filter(this, ATTN_IDLE );

	CSoundParameters params;
	if ( GetParametersForSound( "Headcrab.Alert", params, NULL ) )
	{
		EmitSound_t ep( params );

		ep.m_flVolume = GetSoundVolume();
		ep.m_nPitch = GetVoicePitch();

		EmitSound( filter, entindex(), ep );
	}
}

//=========================================================
// PainSound
//=========================================================
void CNPC_Headcrab::PainSound( const CTakeDamageInfo &info )
{
	CPASAttenuationFilter filter(this, ATTN_IDLE );

	CSoundParameters params;
	if ( GetParametersForSound( "Headcrab.Pain", params, NULL ) )
	{
		EmitSound_t ep( params );

		ep.m_flVolume = GetSoundVolume();
		ep.m_nPitch = GetVoicePitch();

		EmitSound( filter, entindex(), ep );
	}
}

//=========================================================
// AttackSound
//=========================================================
void CNPC_Headcrab::AttackSound( void )
{
	CPASAttenuationFilter filter(this, ATTN_IDLE );

	CSoundParameters params;
	if ( GetParametersForSound( "Headcrab.Attack", params, NULL ) )
	{
		EmitSound_t ep( params );

		ep.m_flVolume = GetSoundVolume();
		ep.m_nPitch = GetVoicePitch();

		EmitSound( filter, entindex(), ep );
	}
}

//=========================================================
// DeathSound
//=========================================================
void CNPC_Headcrab::DeathSound( const CTakeDamageInfo &info )
{
	CPASAttenuationFilter filter(this, ATTN_IDLE );

	CSoundParameters params;
	if ( GetParametersForSound( "Headcrab.Die", params, NULL ) )
	{
		EmitSound_t ep( params );

		ep.m_flVolume = GetSoundVolume();
		ep.m_nPitch = GetVoicePitch();

		EmitSound( filter, entindex(), ep );
	}
}

//=========================================================
// BiteSound
//=========================================================
void CNPC_Headcrab::BiteSound( void )
{
	CPASAttenuationFilter filter( this, ATTN_IDLE );

	CSoundParameters params;
	if ( GetParametersForSound( "Headcrab.Bite", params, NULL ) )
	{
		EmitSound_t ep( params );

		ep.m_flVolume = GetSoundVolume();
		ep.m_nPitch = GetVoicePitch();

		EmitSound( filter, entindex(), ep );
	}
}

//=========================================================
// TranslateSchedule - Was named "GetScheduleOfType" in GoldSource - Theuaredead`
//=========================================================
int CNPC_Headcrab::TranslateSchedule( int scheduleType )
{
	switch( scheduleType )
	{
		case SCHED_RANGE_ATTACK1:
			{
			return SCHED_HEADCRAB_RANGE_ATTACK1;
			}
			break;
	}

	return BaseClass::TranslateSchedule( scheduleType );
}

// Baby Headcrab starts here in GoldSource! - Theuaredead`

class CNPC_BabyCrab : public CNPC_Headcrab
{
	DECLARE_CLASS( CNPC_BabyCrab, CNPC_Headcrab );
public:
	void Spawn( void );
	void Precache( void );

	//New for Source - Theuaredead`
	unsigned int PhysicsSolidMaskForEntity( void ) const;

	float MaxYawSpeed( void );
	float GetDamageAmount( void ) { return sk_headcrab_dmg_bite.GetFloat() * 0.3; };
	int RangeAttack1Conditions ( float flDot, float flDist );
	int TranslateSchedule ( int scheduleType );
	virtual int GetVoicePitch( void ) { return PITCH_NORM + random->RandomInt( 40,50 ); }
	virtual float GetSoundVolume( void ) { return 0.8; }
};
LINK_ENTITY_TO_CLASS( monster_babycrab, CNPC_BabyCrab );

//New for Source - Theuaredead`
unsigned int CNPC_BabyCrab::PhysicsSolidMaskForEntity( void ) const
{
	unsigned int iMask = BaseClass::PhysicsSolidMaskForEntity();

	iMask &= ~CONTENTS_MONSTERCLIP;

	return iMask;
}

void CNPC_BabyCrab::Spawn( void )
{
	CNPC_Headcrab::Spawn();
	SetModel( "models/baby_headcrab.mdl" );
	m_nRenderMode = kRenderTransTexture;
	//Converted to the last value in SetRenderColor, it seems. - Theuaredead`
	//pev->renderamt = 192;
	SetRenderColor( 255, 255, 255, 192 );
	UTIL_SetSize(this, Vector(-12, -12, 0), Vector(12, 12, 24));
	
	m_iHealth	  = sk_headcrab_health.GetFloat() * 0.25;	// less health than full grown
}

void CNPC_BabyCrab::Precache( void )
{
	PrecacheModel( "models/baby_headcrab.mdl" );
	CNPC_Headcrab::Precache();
}

float CNPC_BabyCrab::MaxYawSpeed ( void )
{
	int flYS;

	flYS = 120 / 10;

	return flYS;
}

//used to be CheckRangeAttack1 - Theuaredead
int CNPC_BabyCrab::RangeAttack1Conditions( float flDot, float flDist )
{
	if ( GetFlags() & FL_ONGROUND )
	{
		if ( GetGroundEntity() && ( GetGroundEntity()->GetFlags() & ( FL_CLIENT | FL_NPC ) ) )
			return COND_CAN_RANGE_ATTACK1;

		// A little less accurate, but jump from closer
		if ( flDist <= 256 && flDot >= 0.65 )
			return COND_CAN_RANGE_ATTACK1;
	}

	return COND_NONE;
}

//Was GetScheduleOfType
int CNPC_BabyCrab::TranslateSchedule ( int scheduleType )
{
	switch( scheduleType )
	{
		case SCHED_FAIL:	// If you fail, try to jump!
			if ( GetEnemy() != NULL )
				return SCHED_FAST_HEADCRAB_RANGE_ATTACK1;
		break;

		case SCHED_RANGE_ATTACK1:
		{
			return SCHED_FAST_HEADCRAB_RANGE_ATTACK1;
		}
		break;
	}

	return CNPC_Headcrab::TranslateSchedule( scheduleType );
}
