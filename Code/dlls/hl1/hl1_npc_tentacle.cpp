//========= Copyright � 1996-2005, Valve Corporation, All rights reserved. ============//
//
// Purpose: Bullseyes act as targets for other NPC's to attack and to trigger
//			events 
//
// $Workfile:     $
// $Date:         $
//
//-----------------------------------------------------------------------------
// $Log: $
//
// $NoKeywords: $
//=============================================================================//

/*

	h_tentacle.cpp - silo of death tentacle monster (half life)

*/

#include	"cbase.h"
#include	"AI_Default.h"
#include	"AI_Task.h"
#include	"AI_Schedule.h"
#include	"AI_Node.h"
#include	"AI_Hull.h"
#include	"AI_Hint.h"
#include	"AI_Route.h"
#include	"AI_Senses.h"
#include	"AI_Motor.h"
#include	"soundent.h"
#include	"game.h"
#include	"NPCEvent.h"
#include	"EntityList.h"
#include	"activitylist.h"
#include	"animation.h"
#include	"basecombatweapon.h"
#include	"IEffects.h"
#include	"vstdlib/random.h"
#include	"engine/IEngineSound.h"
#include	"ammodef.h"
#include	"hl1_ai_basenpc.h"
#include	"studio.h"			//hitbox parsing
#include	"collisionutils.h"	//ComputeSeparatingPlane

#include	"physics_bone_follower.h"	//For BoneFollowerManager

#define ACT_T_IDLE		1010
Activity ACT_1010;
Activity ACT_1011;
Activity ACT_1012;
Activity ACT_1013;

#define ACT_T_TAP		1020
Activity ACT_1020;
Activity ACT_1021;
Activity ACT_1022;
Activity ACT_1023;

#define ACT_T_STRIKE	1030
Activity ACT_1030;
Activity ACT_1031;
Activity ACT_1032;
Activity ACT_1033;

#define ACT_T_REARIDLE	1040
Activity ACT_1040;
Activity ACT_1041;
Activity ACT_1042;
Activity ACT_1043;
Activity ACT_1044;

class CNPC_Tentacle : public CHL1BaseNPC
{
	DECLARE_CLASS( CNPC_Tentacle, CHL1BaseNPC );
public:

	CNPC_Tentacle();

	void Spawn( );
	void Precache( );
	bool KeyValue( const char *szKeyName, const char *szValue );

	DECLARE_DATADESC();
	DEFINE_CUSTOM_AI;

	// not in GoldSource - Theuaredead`
	//bool QueryHearSound( CSound *pSound ) { return true; } // Tentacle isn't picky.

	// Don't allow the tentacle to go across transitions!!!
	virtual int	ObjectCaps( void ) { return (BaseClass::ObjectCaps() | FCAP_NOTIFY_ON_TRANSITION); }

	void SetObjectCollisionBox( void )
	{
		//pev->absmin = pev->origin + Vector(-400, -400, 0);
		//pev->absmax = pev->origin + Vector(400, 400, 850);

		Vector vMins = GetAbsOrigin() + Vector(-400, -400, 0);
		Vector vMaxs = GetAbsOrigin() + Vector(400, 400, 850);
		SetCollisionBounds( vMins, vMaxs );
	}
	
	void Cycle ( void );
	void CommandUse( CBaseEntity *pActivator, CBaseEntity *pCaller, USE_TYPE useType, float value );
	void Start ( void );
	void DieThink( void );

	void Test( void );

	void HitTouch( CBaseEntity *pOther );

	float HearingSensitivity( void ) { return 2.0; };

	// Was TakeDamage - Theuaredead`
	int OnTakeDamage_Alive( const CTakeDamageInfo &inputInfo );
	void HandleAnimEvent( animevent_t *pEvent );
	void Killed( CBaseEntity *pevAttacker, int iGib );

	// was GetIdealState - Theuaredead`
	NPC_STATE SelectIdealState ( void ) { return NPC_STATE_IDLE; };
	int CanPlaySequence( bool fDisregardState ) { return TRUE; };

	Class_T Classify ( void );

	int Level( float dz );
	int MyLevel( void );
	float MyHeight( void );

	//float HearingSensitivity( void ) { return 2.0; };
	bool CreateVPhysics( void );
	//void UpdateOnRemove( void );

	float m_flInitialYaw;
	int m_iGoalAnim;
	int m_iLevel;
	int m_iDir;
	float m_flFramerateAdj;
	float m_flSoundYaw;
	int m_iSoundLevel;
	float m_flSoundTime;
	float m_flSoundRadius;
	int m_iHitDmg;
	float m_flHitTime;

	float m_flTapRadius;

	float m_flNextSong;
	static int g_fFlySound;
	static int g_fSquirmSound;

	float m_flMaxYaw;
	int m_iTapSound;

	Vector m_vecPrevSound;
	float m_flPrevSoundTime;

	static const char *pHitSilo[];
	static const char *pHitDirt[];
	static const char *pHitWater[];

	//float	MaxYawSpeed( void ) { 	return 18.0f;	}

	bool	HeardAnything( void );


	//I'll comment this out for now - Theuaredead
	//CBoneFollowerManager	m_BoneFollowerManager;


};

int CNPC_Tentacle::g_fFlySound;
int CNPC_Tentacle::g_fSquirmSound;

LINK_ENTITY_TO_CLASS( monster_tentacle, CNPC_Tentacle );

// stike sounds
#define TE_NONE -1
#define TE_SILO 0
#define TE_DIRT 1
#define TE_WATER 2

BEGIN_DATADESC( CNPC_Tentacle )
	DEFINE_FIELD( m_flInitialYaw, FIELD_FLOAT ),
	DEFINE_FIELD( m_iGoalAnim, FIELD_INTEGER ),
	DEFINE_FIELD( m_iLevel, FIELD_INTEGER ),
	DEFINE_FIELD( m_iDir, FIELD_INTEGER ),
	DEFINE_FIELD( m_flFramerateAdj, FIELD_FLOAT ),
	DEFINE_FIELD( m_flSoundYaw, FIELD_FLOAT ),
	DEFINE_FIELD( m_iSoundLevel, FIELD_INTEGER ),
	DEFINE_FIELD( m_flSoundTime, FIELD_TIME ),
	DEFINE_FIELD( m_flSoundRadius, FIELD_FLOAT ),
	DEFINE_FIELD( m_iHitDmg, FIELD_INTEGER ),
	DEFINE_FIELD( m_flHitTime, FIELD_TIME ),
	DEFINE_FIELD( m_flTapRadius, FIELD_FLOAT ),
	DEFINE_FIELD( m_flNextSong, FIELD_TIME ),
	DEFINE_FIELD( m_iTapSound, FIELD_INTEGER ),
	DEFINE_FIELD( m_flMaxYaw, FIELD_FLOAT ),
	DEFINE_FIELD( m_vecPrevSound, FIELD_POSITION_VECTOR ),
	DEFINE_FIELD( m_flPrevSoundTime, FIELD_TIME ),

	// These arent here in the GoldSource version - Theuaredead`
	//DEFINE_THINKFUNC( Start ),
	//DEFINE_THINKFUNC( Cycle ),
	//DEFINE_ENTITYFUNC( HitTouch ),
END_DATADESC()

// animation sequence aliases 
typedef enum
{
	TENTACLE_ANIM_Pit_Idle,

	TENTACLE_ANIM_rise_to_Temp1,
	TENTACLE_ANIM_Temp1_to_Floor,
	TENTACLE_ANIM_Floor_Idle,
	TENTACLE_ANIM_Floor_Fidget_Pissed,
	TENTACLE_ANIM_Floor_Fidget_SmallRise,
	TENTACLE_ANIM_Floor_Fidget_Wave,
	TENTACLE_ANIM_Floor_Strike,
	TENTACLE_ANIM_Floor_Tap,
	TENTACLE_ANIM_Floor_Rotate,
	TENTACLE_ANIM_Floor_Rear,
	TENTACLE_ANIM_Floor_Rear_Idle,
	TENTACLE_ANIM_Floor_to_Lev1,

	TENTACLE_ANIM_Lev1_Idle,
	TENTACLE_ANIM_Lev1_Fidget_Claw,
	TENTACLE_ANIM_Lev1_Fidget_Shake,
	TENTACLE_ANIM_Lev1_Fidget_Snap,
	TENTACLE_ANIM_Lev1_Strike,
	TENTACLE_ANIM_Lev1_Tap,
	TENTACLE_ANIM_Lev1_Rotate,
	TENTACLE_ANIM_Lev1_Rear,
	TENTACLE_ANIM_Lev1_Rear_Idle,
	TENTACLE_ANIM_Lev1_to_Lev2,

	TENTACLE_ANIM_Lev2_Idle,
	TENTACLE_ANIM_Lev2_Fidget_Shake,
	TENTACLE_ANIM_Lev2_Fidget_Swing,
	TENTACLE_ANIM_Lev2_Fidget_Tut,
	TENTACLE_ANIM_Lev2_Strike,
	TENTACLE_ANIM_Lev2_Tap,
	TENTACLE_ANIM_Lev2_Rotate,
	TENTACLE_ANIM_Lev2_Rear,
	TENTACLE_ANIM_Lev2_Rear_Idle,
	TENTACLE_ANIM_Lev2_to_Lev3,

	TENTACLE_ANIM_Lev3_Idle,
	TENTACLE_ANIM_Lev3_Fidget_Shake,
	TENTACLE_ANIM_Lev3_Fidget_Side,
	TENTACLE_ANIM_Lev3_Fidget_Swipe,
	TENTACLE_ANIM_Lev3_Strike,
	TENTACLE_ANIM_Lev3_Tap,
	TENTACLE_ANIM_Lev3_Rotate,
	TENTACLE_ANIM_Lev3_Rear,
	TENTACLE_ANIM_Lev3_Rear_Idle,

	TENTACLE_ANIM_Lev1_Door_reach,

	TENTACLE_ANIM_Lev3_to_Engine,
	TENTACLE_ANIM_Engine_Idle,
	TENTACLE_ANIM_Engine_Sway,
	TENTACLE_ANIM_Engine_Swat,
	TENTACLE_ANIM_Engine_Bob,
	TENTACLE_ANIM_Engine_Death1,
	TENTACLE_ANIM_Engine_Death2,
	TENTACLE_ANIM_Engine_Death3,

	TENTACLE_ANIM_none
} TENTACLE_ANIM;





//=========================================================
// Classify - indicates this monster's place in the 
// relationship table.
//=========================================================
Class_T CNPC_Tentacle::Classify ( void ) 
{ 
	return CLASS_ALIEN_MONSTER;	
}

//
// Tentacle Spawn
//
void CNPC_Tentacle::Spawn( )
{
	Precache( );

	SetSolid( SOLID_BBOX );
	//Necessary for TestCollision to be called for hitbox ray hits
	AddSolidFlags( FSOLID_CUSTOMRAYTEST );

	SetMoveType( MOVETYPE_FLY );
	ClearEffects();
	m_iHealth			= 75;
	SetSequence( 0 );

	SetModel( "models/tentacle2.mdl" );
	UTIL_SetSize( this, Vector( -32, -32, 0 ), Vector( 32, 32, 64 ) );

	// Use our hitboxes to determine our render bounds
	CollisionProp()->SetSurroundingBoundsType( USE_HITBOXES );

	m_takedamage		= DAMAGE_AIM;
	AddFlag( FL_NPC );
	
	m_bloodColor		= BLOOD_COLOR_GREEN;

	//SetThink( Start );
	//SetTouch( HitTouch );
	//SetUse( CommandUse );

	SetThink( &CNPC_Tentacle::Start );
	SetTouch( &CNPC_Tentacle::HitTouch );
	SetUse( &CNPC_Tentacle::CommandUse );
	SetNextThink( gpGlobals->curtime + 0.2 );

	ResetSequenceInfo( );
	m_iDir = 1;

	// blah blah something something yawspeed, convert - Theuaredead`
	//pev->yaw_speed = 18;
	m_flInitialYaw = GetAbsAngles().y;
	GetMotor()->SetIdealYawAndUpdate( m_flInitialYaw );
	
	g_fFlySound = FALSE;
	g_fSquirmSound = FALSE;

	//why is this 200? it was 20 - Theuaredead`
	m_iHitDmg = 20;

	if (m_flMaxYaw <= 0)
		//yaw is devided by 10 because source is 10 times faster! - Theuaredead`
		m_flMaxYaw = 65 / 10;

	m_NPCState = NPC_STATE_IDLE;

	UTIL_SetOrigin( this, GetAbsOrigin() );

	CreateVPhysics();

	AddEffects( EF_NOSHADOW );
}

void CNPC_Tentacle::Precache( )
{
	PrecacheModel("models/tentacle2.mdl");

	PrecacheScriptSound( "Tentacle.Flies" );
	PrecacheScriptSound( "Tentacle.Squirm" );
	PrecacheScriptSound( "Tentacle.Sing" );
	PrecacheScriptSound( "Tentacle.HitDirt" );
	PrecacheScriptSound( "Tentacle.Swing" );
	PrecacheScriptSound( "Tentacle.Search" );
	PrecacheScriptSound( "Tentacle.Roar" );
	PrecacheScriptSound( "Tentacle.Alert" );

	//worry about this later if needed - Theuaredead`
	//PRECACHE_SOUND_ARRAY( pHitSilo );
	//PRECACHE_SOUND_ARRAY( pHitDirt );
	//PRECACHE_SOUND_ARRAY( pHitWater );

	BaseClass::Precache();
}

CNPC_Tentacle::CNPC_Tentacle()
{
	m_flMaxYaw = 65 / 10;
	m_iTapSound = 0;
}

bool CNPC_Tentacle::KeyValue( const char *szKeyName, const char *szValue  )
{
	if ( FStrEq( szKeyName, "sweeparc") )
	{
		 m_flMaxYaw = atof( szValue ) / 2.0;
		 return true;
	}
	else if (FStrEq( szKeyName, "sound"))
	{
		 m_iTapSound = atoi( szValue );
		 return true;
	}
	else
		return BaseClass::KeyValue( szKeyName, szValue );

	//This line didn't exist originally - Theuaredead`
//	return false;
}

int CNPC_Tentacle::Level( float dz )
{
	if (dz < 216)
		return 0;
	if (dz < 408)
		return 1;
	if (dz < 600)
		return 2;
	return 3;
}

float CNPC_Tentacle::MyHeight( )
{
	switch ( MyLevel( ) )
	{
	case 1:
		return 256;
	case 2:
		return 448;
	case 3:
		return 640;
	}
	return 0;
}

int CNPC_Tentacle::MyLevel( )
{
	switch( GetSequence() )
	{
	case TENTACLE_ANIM_Pit_Idle: 
		return -1;

	case TENTACLE_ANIM_rise_to_Temp1:
	case TENTACLE_ANIM_Temp1_to_Floor:
	case TENTACLE_ANIM_Floor_to_Lev1:
		return 0;

	case TENTACLE_ANIM_Floor_Idle:
	case TENTACLE_ANIM_Floor_Fidget_Pissed:
	case TENTACLE_ANIM_Floor_Fidget_SmallRise:
	case TENTACLE_ANIM_Floor_Fidget_Wave:
	case TENTACLE_ANIM_Floor_Strike:
	case TENTACLE_ANIM_Floor_Tap:
	case TENTACLE_ANIM_Floor_Rotate:
	case TENTACLE_ANIM_Floor_Rear:
	case TENTACLE_ANIM_Floor_Rear_Idle:
		return 0;

	case TENTACLE_ANIM_Lev1_Idle:
	case TENTACLE_ANIM_Lev1_Fidget_Claw:
	case TENTACLE_ANIM_Lev1_Fidget_Shake:
	case TENTACLE_ANIM_Lev1_Fidget_Snap:
	case TENTACLE_ANIM_Lev1_Strike:
	case TENTACLE_ANIM_Lev1_Tap:
	case TENTACLE_ANIM_Lev1_Rotate:
	case TENTACLE_ANIM_Lev1_Rear:
	case TENTACLE_ANIM_Lev1_Rear_Idle:
		return 1;

	case TENTACLE_ANIM_Lev1_to_Lev2:
		return 1;

	case TENTACLE_ANIM_Lev2_Idle:
	case TENTACLE_ANIM_Lev2_Fidget_Shake:
	case TENTACLE_ANIM_Lev2_Fidget_Swing:
	case TENTACLE_ANIM_Lev2_Fidget_Tut:
	case TENTACLE_ANIM_Lev2_Strike:
	case TENTACLE_ANIM_Lev2_Tap:
	case TENTACLE_ANIM_Lev2_Rotate:
	case TENTACLE_ANIM_Lev2_Rear:
	case TENTACLE_ANIM_Lev2_Rear_Idle:
		return 2;

	case TENTACLE_ANIM_Lev2_to_Lev3:
		return 2;

	case TENTACLE_ANIM_Lev3_Idle:
	case TENTACLE_ANIM_Lev3_Fidget_Shake:
	case TENTACLE_ANIM_Lev3_Fidget_Side:
	case TENTACLE_ANIM_Lev3_Fidget_Swipe:
	case TENTACLE_ANIM_Lev3_Strike:
	case TENTACLE_ANIM_Lev3_Tap:
	case TENTACLE_ANIM_Lev3_Rotate:
	case TENTACLE_ANIM_Lev3_Rear:
	case TENTACLE_ANIM_Lev3_Rear_Idle:
		return 3;

	case TENTACLE_ANIM_Lev1_Door_reach:
		return -1;
	}
	return -1;
}

void CNPC_Tentacle::Test( void )
{
	SetSequence( TENTACLE_ANIM_Floor_Strike );
	m_flPlaybackRate = 0;
	StudioFrameAdvance( );
	SetNextThink( gpGlobals->curtime + 1.0 );
}

void CNPC_Tentacle::Cycle( void )
{
	// ALERT( at_console, "%s %.2f %d %d\n", STRING( pev->targetname ), pev->origin.z, m_MonsterState, m_IdealMonsterState );
	SetNextThink( gpGlobals->curtime + 0.1 );

	// ALERT( at_console, "%s %d %d %d %f %f\n", STRING( pev->targetname ), pev->sequence, m_iGoalAnim, m_iDir, pev->framerate, pev->health );

	if ( m_NPCState == NPC_STATE_SCRIPT || GetIdealState() == NPC_STATE_SCRIPT)
	{
		SetAbsAngles( QAngle( GetAbsAngles().x, m_flInitialYaw, GetAbsAngles().z ) );
		GetMotor()->SetIdealYaw( m_flInitialYaw );	
		RemoveIgnoredConditions();
		NPCThink( );
		m_iGoalAnim = TENTACLE_ANIM_Pit_Idle;
		return;
	}
	
	StudioFrameAdvance();
	DispatchAnimEvents( this );

	GetMotor()->UpdateYaw( MaxYawSpeed() );
	
	CSound *pSound;

	GetSenses()->Listen();
	//m_BoneFollowerManager.UpdateBoneFollowers();

	// Listen will set this if there's something in my sound list
	if ( HeardAnything() )
		 pSound = GetSenses()->GetClosestSound( false, (SOUND_DANGER|SOUND_COMBAT|SOUND_WORLD|SOUND_PLAYER) );
	else
		 pSound = NULL;

	if ( pSound )
	{
		//NDebugOverlay::Line( EarPosition(), pSound->GetSoundOrigin(), 0, 255, 0, false, 0.2 );

		Vector vecDir;
		if ( gpGlobals->curtime - m_flPrevSoundTime < 0.5 )
		{
			float dt = gpGlobals->curtime - m_flPrevSoundTime;
			vecDir = pSound->GetSoundOrigin() + (pSound->GetSoundOrigin() - m_vecPrevSound) / dt - GetAbsOrigin();
		}
		else
		{
			vecDir = pSound->GetSoundOrigin() - GetAbsOrigin();
		}

		m_flPrevSoundTime = gpGlobals->curtime;
		m_vecPrevSound = pSound->GetSoundOrigin();

		m_flSoundYaw = VecToYaw ( vecDir ) - m_flInitialYaw;
		m_iSoundLevel = Level( vecDir.z );

		// Would we devide sound yaw? test it? - Theuaredead`
		// Gonna see... - Theuaredead`
		if (m_flSoundYaw < -(180 / 10))
			m_flSoundYaw += 360 / 10;
		if (m_flSoundYaw > 180 / 10)
			m_flSoundYaw -= 360 / 10;

		// ALERT( at_console, "sound %d %.0f\n", m_iSoundLevel, m_flSoundYaw );
		if (m_flSoundTime < gpGlobals->curtime)
		{
			// play "I hear new something" sound
			 UTIL_EmitAmbientSound( GetSoundSourceIndex(), GetAbsOrigin() + Vector( 0, 0, MyHeight()), "Tentacle.Alert", 1.0, SNDLVL_GUNFIRE, 0, 100);
		}
		m_flSoundTime = gpGlobals->curtime + random->RandomFloat( 5.0, 10.0 );
	}

	// clip ideal_yaw
	float dy = m_flSoundYaw;
	switch( GetSequence() )
	{
	case TENTACLE_ANIM_Floor_Rear:
	case TENTACLE_ANIM_Floor_Rear_Idle:
	case TENTACLE_ANIM_Lev1_Rear:
	case TENTACLE_ANIM_Lev1_Rear_Idle:
	case TENTACLE_ANIM_Lev2_Rear:
	case TENTACLE_ANIM_Lev2_Rear_Idle:
	case TENTACLE_ANIM_Lev3_Rear:
	case TENTACLE_ANIM_Lev3_Rear_Idle:
		if (dy < 0 && dy > -m_flMaxYaw)
			dy = -m_flMaxYaw;
		if (dy > 0 && dy < m_flMaxYaw)
			dy = m_flMaxYaw;
		break;
	default:
		if (dy < -m_flMaxYaw)
			dy = -m_flMaxYaw;
		if (dy > m_flMaxYaw)
			dy = m_flMaxYaw;
	}
	GetMotor()->SetIdealYaw( m_flInitialYaw + dy );

	if ( IsSequenceFinished() )
	{
		// ALERT( at_console, "%s done %d %d\n", STRING( pev->targetname ), pev->sequence, m_iGoalAnim );
		if ( m_iHealth <= 1)
		{
			m_iGoalAnim = TENTACLE_ANIM_Pit_Idle;

			if ( GetSequence() == TENTACLE_ANIM_Pit_Idle)
			{
				m_iHealth = 75;
			}
		}
		else if ( m_flSoundTime > gpGlobals->curtime )
		{
			if (m_flSoundYaw >= -(m_flMaxYaw + 30 / 10) && m_flSoundYaw <= (m_flMaxYaw + 30 / 10))
			{
				// strike
				switch ( m_iSoundLevel )
				{
					case 0:
						m_iGoalAnim = SelectWeightedSequence ( ACT_1030 );
						break;
					case 1:
						m_iGoalAnim = SelectWeightedSequence ( ACT_1031 );
						break;
					case 2:
						m_iGoalAnim = SelectWeightedSequence ( ACT_1032 );
						break;
					case 3:
						m_iGoalAnim = SelectWeightedSequence ( ACT_1033 );
						break;
				}
			}
			// I devided these by 10, but I'm not sure what effect I will have... - Theuaredead`
			else if (m_flSoundYaw >= -m_flMaxYaw * 2 / 10 && m_flSoundYaw <= m_flMaxYaw * 2 / 10) 
			{
				// tap
				switch ( m_iSoundLevel )
				{
					case 0:
						m_iGoalAnim = SelectWeightedSequence ( ACT_1020 );
						break;
					case 1:
						m_iGoalAnim = SelectWeightedSequence ( ACT_1021 );
						break;
					case 2:
						m_iGoalAnim = SelectWeightedSequence ( ACT_1022 );
						break;
					case 3:
						m_iGoalAnim = SelectWeightedSequence ( ACT_1023 );
						break;
				}
			}
			else
			{
				// go into rear idle
				switch ( m_iSoundLevel )
				{
					case 0:
						m_iGoalAnim = SelectWeightedSequence ( ACT_1040 );
						break;
					case 1:
						m_iGoalAnim = SelectWeightedSequence ( ACT_1041 );
						break;
					case 2:
						m_iGoalAnim = SelectWeightedSequence ( ACT_1042 );
						break;
					case 3:
						m_iGoalAnim = SelectWeightedSequence ( ACT_1043 );
						break;
					case 4:
						m_iGoalAnim = SelectWeightedSequence ( ACT_1044 );
						break;
				}
			}
		}
		else if ( GetSequence() == TENTACLE_ANIM_Pit_Idle)
		{
			// stay in pit until hear noise
			m_iGoalAnim = TENTACLE_ANIM_Pit_Idle;
		}
		else if ( GetSequence() == m_iGoalAnim)
		{
			if ( MyLevel() >= 0 && gpGlobals->curtime < m_flSoundTime)
			{
				if ( random->RandomInt(0,9) < m_flSoundTime - gpGlobals->curtime )
				{
					// continue stike
					switch ( m_iSoundLevel )
					{
						case 0:
							m_iGoalAnim = SelectWeightedSequence ( ACT_1030 );
							break;
						case 1:
							m_iGoalAnim = SelectWeightedSequence ( ACT_1031 );
							break;
						case 2:
							m_iGoalAnim = SelectWeightedSequence ( ACT_1032 );
							break;
						case 3:
							m_iGoalAnim = SelectWeightedSequence ( ACT_1033 );
							break;
					}
				}
				else
				{
					// tap
					switch ( m_iSoundLevel )
					{
						case 0:
							m_iGoalAnim = SelectWeightedSequence ( ACT_1020 );
							break;
						case 1:
							m_iGoalAnim = SelectWeightedSequence ( ACT_1021 );
							break;
						case 2:
							m_iGoalAnim = SelectWeightedSequence ( ACT_1022 );
							break;
						case 3:
							m_iGoalAnim = SelectWeightedSequence ( ACT_1023 );
							break;
					}
				}
			}
			else if ( MyLevel( ) < 0 )
			{
				m_iGoalAnim = SelectWeightedSequence( ACT_1010 );
			}
			else
			{
				if (m_flNextSong < gpGlobals->curtime)
				{
					// play "I hear new something" sound
					CPASAttenuationFilter filter( this );
					EmitSound( filter, entindex(), "Tentacle.Sing" );

					m_flNextSong = gpGlobals->curtime + random->RandomFloat( 10, 20 );
				}

				if (random->RandomInt(0,15) == 0)
				{
					// idle on new level
					switch ( random->RandomInt( 0, 3 ) )
					{
						case 0:
							m_iGoalAnim = SelectWeightedSequence ( ACT_1010 );
							break;
						case 1:
							m_iGoalAnim = SelectWeightedSequence ( ACT_1011 );
							break;
						case 2:
							m_iGoalAnim = SelectWeightedSequence ( ACT_1012 );
							break;
						case 3:
							m_iGoalAnim = SelectWeightedSequence ( ACT_1013 );
							break;
					}
				}
				else if ( random->RandomInt( 0, 3 ) == 0 )
				{
					// tap
					switch ( MyLevel() )
					{
						case 0:
							m_iGoalAnim = SelectWeightedSequence ( ACT_1020 );
							break;
						case 1:
							m_iGoalAnim = SelectWeightedSequence ( ACT_1021 );
							break;
						case 2:
							m_iGoalAnim = SelectWeightedSequence ( ACT_1022 );
							break;
						case 3:
							m_iGoalAnim = SelectWeightedSequence ( ACT_1023 );
							break;
					}
				}
				else
				{
					// idle
					switch ( MyLevel() )
					{
						case 0:
							m_iGoalAnim = SelectWeightedSequence ( ACT_1010 );
							break;
						case 1:
							m_iGoalAnim = SelectWeightedSequence ( ACT_1011 );
							break;
						case 2:
							m_iGoalAnim = SelectWeightedSequence ( ACT_1012 );
							break;
						case 3:
							m_iGoalAnim = SelectWeightedSequence ( ACT_1013 );
							break;
					}
				}
			}
			if (m_flSoundYaw < 0)
				m_flSoundYaw += random->RandomFloat( 2, 8 );
			else
				m_flSoundYaw -= random->RandomFloat( 2, 8 );
		}

		SetSequence( FindTransitionSequence( GetSequence(), m_iGoalAnim, &m_iDir ) );
		

		if (m_iDir > 0)
		{
			SetCycle( 0 );
		}
		else
		{
			m_iDir = -1; // just to safe
			SetCycle( 1.0f );
		}

		ResetSequenceInfo( );

		m_flFramerateAdj = random->RandomFloat( -0.2, 0.2 );
		m_flPlaybackRate = m_iDir * 1.0 + m_flFramerateAdj;

		switch( GetSequence() )
		{
		case TENTACLE_ANIM_Floor_Tap:
		case TENTACLE_ANIM_Lev1_Tap:
		case TENTACLE_ANIM_Lev2_Tap:
		case TENTACLE_ANIM_Lev3_Tap:
			{
				Vector vecSrc, v_forward;
				AngleVectors( GetAbsAngles(), &v_forward );

				trace_t tr1, tr2;

				vecSrc = GetAbsOrigin() + Vector( 0, 0, MyHeight() - 4);
				UTIL_TraceLine( vecSrc, vecSrc + v_forward * 512, MASK_NPCSOLID_BRUSHONLY, this, COLLISION_GROUP_NONE, &tr1 );

				vecSrc = GetAbsOrigin() + Vector( 0, 0, MyHeight() + 8);
				UTIL_TraceLine( vecSrc, vecSrc + v_forward * 512, MASK_NPCSOLID_BRUSHONLY, this, COLLISION_GROUP_NONE, &tr2 );

				// ALERT( at_console, "%f %f\n", tr1.flFraction * 512, tr2.flFraction * 512 );

				m_flTapRadius = SetPoseParameter( 0, random->RandomFloat( tr1.fraction * 512, tr2.fraction * 512 ) );
			}
			break;
		default:
			m_flTapRadius = 336; // 400 - 64
			break;
		}
		SetViewOffset( Vector( 0, 0, MyHeight() ) );
		// ALERT( at_console, "seq %d\n", pev->sequence );
	}

	if (m_flPrevSoundTime + 2.0 > gpGlobals->curtime)
	{
		// 1.5 normal speed if hears sounds
		m_flPlaybackRate = m_iDir * 1.5 + m_flFramerateAdj;
	}
	else if (m_flPrevSoundTime + 5.0 > gpGlobals->curtime)
	{
		// slowdown to normal
		m_flPlaybackRate = m_iDir + m_iDir * (5 - (gpGlobals->curtime - m_flPrevSoundTime)) / 2 + m_flFramerateAdj;
	}
}

void CNPC_Tentacle::CommandUse( CBaseEntity *pActivator, CBaseEntity *pCaller, USE_TYPE useType, float value )
{
	// This needs converted...
	/*
	// ALERT( at_console, "%s triggered %d\n", STRING( pev->targetname ), useType ); 
	switch( useType )
	{
	case USE_OFF:
		TakeDamage( DAMAGE_NO );
		SetThink( &CNPC_Tentacle::DieThink );
		m_iGoalAnim = TENTACLE_ANIM_Engine_Death1;
		break;
	case USE_ON:
		if (pActivator)
		{
			// ALERT( at_console, "insert sound\n");
			CSoundEnt::InsertSound ( SOUND_WORLD, GetAbsOrigin(), 1024, 1.0 );
		}
		break;
	case USE_SET:
		break;
	case USE_TOGGLE:
		TakeDamage( DAMAGE_NO );
		SetThink( &CNPC_Tentacle::DieThink );
		m_iGoalAnim = TENTACLE_ANIM_Engine_Idle;
		break;
	}
*/
}

void CNPC_Tentacle::DieThink( void )
{
	//Needs converted! - Theuaredead`
	/*
	//pev->nextthink = gpGlobals-> time + 0.1;
	SetNextThink( gpGlobals->curtime + 0.1 );

	DispatchAnimEvents( this );
	StudioFrameAdvance( );

	// what the fuck would i convert this to? - theuaredead`
	//ChangeYaw( 24 / 10 );

	if ( IsSequenceFinished() )
	{
		if (SetSequence() == m_iGoalAnim)
		{
			switch( m_iGoalAnim )
			{
			case TENTACLE_ANIM_Engine_Idle:
			case TENTACLE_ANIM_Engine_Sway:
			case TENTACLE_ANIM_Engine_Swat:
			case TENTACLE_ANIM_Engine_Bob:
				m_iGoalAnim = TENTACLE_ANIM_Engine_Sway + RANDOM_LONG( 0, 2 );
				break;
			case TENTACLE_ANIM_Engine_Death1:
			case TENTACLE_ANIM_Engine_Death2:
			case TENTACLE_ANIM_Engine_Death3:
				UTIL_Remove( this );
				return;
			}
		}

		// ALERT( at_console, "%d : %d => ", pev->sequence, m_iGoalAnim );
		SetSequence() = FindTransition( SetSequence(), m_iGoalAnim, &m_iDir );
		// ALERT( at_console, "%d\n", pev->sequence );

		if (m_iDir > 0)
		{
			SetCycle( 0 );
		}
		else
		{
			SetCycle( 255 );
		}
		ResetSequenceInfo( );

		float dy;
		switch( pev->sequence )
		{
		case TENTACLE_ANIM_Floor_Rear:
		case TENTACLE_ANIM_Floor_Rear_Idle:
		case TENTACLE_ANIM_Lev1_Rear:
		case TENTACLE_ANIM_Lev1_Rear_Idle:
		case TENTACLE_ANIM_Lev2_Rear:
		case TENTACLE_ANIM_Lev2_Rear_Idle:
		case TENTACLE_ANIM_Lev3_Rear:
		case TENTACLE_ANIM_Lev3_Rear_Idle:
		case TENTACLE_ANIM_Engine_Idle:
		case TENTACLE_ANIM_Engine_Sway:
		case TENTACLE_ANIM_Engine_Swat:
		case TENTACLE_ANIM_Engine_Bob:
		case TENTACLE_ANIM_Engine_Death1:
		case TENTACLE_ANIM_Engine_Death2:
		case TENTACLE_ANIM_Engine_Death3:
			m_flPlaybackRate = RANDOM_FLOAT( m_iDir - 0.2, m_iDir + 0.2 );
			dy = 180;
			break;
		default:
			m_flPlaybackRate = 1.5;
			dy = 0;
			break;
		}
		pev->ideal_yaw = m_flInitialYaw + dy;
	}
	*/
}

void CNPC_Tentacle::HandleAnimEvent( animevent_t *pEvent )
{
	switch( pEvent->event )
	{
	case 1:	// bang 
		{
			Vector vecSrc;
			QAngle angAngles;
			GetAttachment( "0", vecSrc, angAngles );

			// Vector vecSrc = GetAbsOrigin() + m_flTapRadius * Vector( cos( GetAbsAngles().y * (3.14192653 / 180.0) ), sin( GetAbsAngles().y * (M_PI / 180.0) ), 0.0 );

			// vecSrc.z += MyHeight( );

			switch( m_iTapSound )
			{
			case TE_SILO:
				UTIL_EmitAmbientSound( GetSoundSourceIndex(), vecSrc, "Tentacle.HitSilo", 1.0, SNDLVL_GUNFIRE, 0, 100);
				break;
			case TE_NONE:
				break;
			case TE_DIRT:
				UTIL_EmitAmbientSound( GetSoundSourceIndex(), vecSrc, "Tentacle.HitDirt", 1.0, SNDLVL_GUNFIRE, 0, 100);
				break;
			case TE_WATER:
				UTIL_EmitAmbientSound( GetSoundSourceIndex(), vecSrc, "Tentacle.HitWater", 1.0, SNDLVL_GUNFIRE, 0, 100);
				break;
			}
		}
		break;

	case 3: // start killing swing
		m_iHitDmg = 200;
		break;

	case 4: // end killing swing
		m_iHitDmg = 25;
		break;

	case 5: // just "whoosh" sound
		break;

	case 2:	// tap scrape
	case 6: // light tap
		{
			Vector vecSrc = GetAbsOrigin() + m_flTapRadius * Vector( cos( GetAbsAngles().y * (M_PI / 180.0) ), sin( GetAbsAngles().y * (M_PI / 180.0) ), 0.0 );

			vecSrc.z += MyHeight( );

			float flVol = random->RandomFloat( 0.3, 0.5 );

			switch( m_iTapSound )
			{
			case TE_SILO:
				UTIL_EmitAmbientSound( GetSoundSourceIndex(), vecSrc, "Tentacle.HitSilo", flVol, SNDLVL_GUNFIRE, 0, 100);
				break;
			case TE_NONE:
				break;
			case TE_DIRT:
				UTIL_EmitAmbientSound( GetSoundSourceIndex(), vecSrc, "Tentacle.HitDirt", flVol, SNDLVL_GUNFIRE, 0, 100);
				break;
			case TE_WATER:
				UTIL_EmitAmbientSound( GetSoundSourceIndex(), vecSrc, "Tentacle.HitWater", flVol, SNDLVL_GUNFIRE, 0, 100);
				break;
			}
		}
		break;


	case 7: // roar
		UTIL_EmitAmbientSound( GetSoundSourceIndex(), GetAbsOrigin() + Vector( 0, 0, MyHeight()), "Tentacle.Roar", 1.0, SNDLVL_GUNFIRE, 0, 100);
		break;

	case 8: // search
		UTIL_EmitAmbientSound( GetSoundSourceIndex(), GetAbsOrigin() + Vector( 0, 0, MyHeight()), "Tentacle.Search", 1.0, SNDLVL_GUNFIRE, 0, 100);
		break;

	case 9: // swing
		UTIL_EmitAmbientSound( GetSoundSourceIndex(), GetAbsOrigin() + Vector( 0, 0, MyHeight()), "Tentacle.Swing", 1.0, SNDLVL_GUNFIRE, 0, 100);
		break;
	default:
		BaseClass::HandleAnimEvent( pEvent );
	}
}

void CNPC_Tentacle::Start( void )
{
	SetThink( &CNPC_Tentacle::Cycle );

	CPASAttenuationFilter filter( this );

	if ( !g_fFlySound )
	{
		EmitSound( filter, entindex(), "Tentacle.Flies" );
		g_fFlySound = TRUE;
	}
	else if ( !g_fSquirmSound )
	{
		EmitSound( filter, entindex(), "Tentacle.Squirm" );
		g_fSquirmSound = TRUE;
	}
	
	SetNextThink( gpGlobals->curtime + 0.1 );
}

void CNPC_Tentacle::HitTouch( CBaseEntity *pOther )
{
	if (m_flHitTime > gpGlobals->curtime)
		return;

	// only look at the ones where the player hit me
	if( pOther == NULL || pOther->GetModelIndex() == GetModelIndex() || ( pOther->GetSolidFlags() & FSOLID_TRIGGER ) )
		 return;

	//Right now the BoneFollower will always be hit in box 0, and 
	//will pass that to us. Make *any* touch by the physics objects a kill
	//as the ragdoll only covers the top portion of the tentacle.
	CTakeDamageInfo info( this, this, m_iHitDmg, DMG_CLUB );
	CalculateMeleeDamageForce( &info, (pOther->GetAbsOrigin() - GetAbsOrigin()), pOther->GetAbsOrigin() );
	pOther->TakeDamage( info );

	m_flHitTime = gpGlobals->curtime + 0.5;

	//GoldSource Code - Theuaredead`
	/*
	TraceResult tr = UTIL_GetGlobalTrace( );

	if (pOther->pev->modelindex == pev->modelindex)
		return;

	if (m_flHitTime > gpGlobals->time)
		return;

	// only look at the ones where the player hit me
	if (tr.pHit == NULL || tr.pHit->v.modelindex != pev->modelindex)
		return;

	if (tr.iHitgroup >= 3)
	{
		pOther->TakeDamage( pev, pev, m_iHitDmg, DMG_CRUSH );
		// ALERT( at_console, "wack %3d : ", m_iHitDmg );
	}
	else if (tr.iHitgroup != 0)
	{
		pOther->TakeDamage( pev, pev, 20, DMG_CRUSH );
		// ALERT( at_console, "tap  %3d : ", 20 );
	}
	else
	{
		return; // Huh?
	}

	m_flHitTime = gpGlobals->time + 0.5;

	// ALERT( at_console, "%s : ", STRING( tr.pHit->v.classname ) );

	// ALERT( at_console, "%.0f : %s : %d\n", pev->angles.y, STRING( pOther->pev->classname ), tr.iHitgroup );
	*/
}

int CNPC_Tentacle::OnTakeDamage_Alive( const CTakeDamageInfo &info )
{
	CTakeDamageInfo i = info;

	//Don't allow the tentacle to die. Instead set health to 1, so we can do our own death and rebirth
	if( (int)i.GetDamage() >= m_iHealth )
	{
		i.SetDamage( 0.0f );
		m_iHealth = 1;
	}

	//GoldSource code - Theuaredead`
/*
	if (flDamage > pev->health)
	{
		pev->health = 1;
	}
	else
	{
		pev->health -= flDamage;
	}
	return 1;
*/

	return BaseClass::OnTakeDamage( i );
}

void CNPC_Tentacle::Killed( CBaseEntity *pevAttacker, int iGib )
{
	m_iGoalAnim = TENTACLE_ANIM_Pit_Idle;
	return;
}

//This exists because COND_HEAR_SOUND doesn't exist. - Theuaredead`
bool CNPC_Tentacle::HeardAnything( void )
{
	if ( HasCondition( COND_HEAR_DANGER ) || // I remove a bunch of sounds from here on purpose. Talk to me if you're changing this!(sjb)
		 HasCondition( COND_HEAR_COMBAT ) ||
		 HasCondition( COND_HEAR_WORLD )  ||
		 HasCondition( COND_HEAR_PLAYER ) )
		 return true;

	return false;
}

//These don't exist in GoldSource! - Theuaredead`
/*
// Crane bones that have physics followers
static const char *pTentacleFollowerBoneNames[] =
{
	"Bone08",
	"Bone09"
};

void CNPC_Tentacle::UpdateOnRemove( void )
{
	m_BoneFollowerManager.DestroyBoneFollowers();
	BaseClass::UpdateOnRemove();
}
*/


// Likely for Collision, new to Source. - Theuaredead`

bool CNPC_Tentacle::CreateVPhysics( void )
{
	BaseClass::CreateVPhysics();

	IPhysicsObject *pPhysics = VPhysicsGetObject();
	if( pPhysics )
	{
		unsigned short flags = pPhysics->GetCallbackFlags();

		flags |= CALLBACK_GLOBAL_TOUCH;

		pPhysics->SetCallbackFlags( flags );
	}
	//I'll comment this out for now - Theuaredead
	//m_BoneFollowerManager.InitBoneFollowers( this, ARRAYSIZE(pTentacleFollowerBoneNames), pTentacleFollowerBoneNames );

	return true;
}

//------------------------------------------------------------------------------
//
// Schedules - This didn't exist in HL, but I'm assuming it relates to the new way some of these anims are handled. - Theuaredead`
//
//------------------------------------------------------------------------------

AI_BEGIN_CUSTOM_NPC( monster_tentacle, CNPC_Tentacle )

	DECLARE_ACTIVITY( ACT_1010 )
	DECLARE_ACTIVITY( ACT_1011 )
	DECLARE_ACTIVITY( ACT_1012 )
	DECLARE_ACTIVITY( ACT_1013 )

	DECLARE_ACTIVITY( ACT_1020 )
	DECLARE_ACTIVITY( ACT_1021 )
	DECLARE_ACTIVITY( ACT_1022 )
	DECLARE_ACTIVITY( ACT_1023 )

	DECLARE_ACTIVITY( ACT_1030 )
	DECLARE_ACTIVITY( ACT_1031 )
	DECLARE_ACTIVITY( ACT_1032 )
	DECLARE_ACTIVITY( ACT_1033 )

	DECLARE_ACTIVITY( ACT_1040 )
	DECLARE_ACTIVITY( ACT_1041 )
	DECLARE_ACTIVITY( ACT_1042 )
	DECLARE_ACTIVITY( ACT_1043 )
	DECLARE_ACTIVITY( ACT_1044 )

AI_END_CUSTOM_NPC()


//Tentacle Maw goes here...
//It's still with-in the HL codebase but is completely unused with-in the game itself.
//The model doesn't exist, but monster_tentaclemaw is still spawnable with-in Half-Life.
//I haven't converted over any of this code AT ALL, but I will put it in for refernece or to be ported over.
//It's not top priority as it won't do anything in HLS beside be an error model, but whatever. - Theuaredead`

/*
class CTentacleMaw : public CBaseMonster
{
public:
	void Spawn( );
	void Precache( );
};

LINK_ENTITY_TO_CLASS( monster_tentaclemaw, CTentacleMaw );

//
// Tentacle Spawn
//
void CTentacleMaw :: Spawn( )
{
	Precache( );
	SET_MODEL(ENT(pev), "models/maw.mdl");
	UTIL_SetSize(pev, Vector(-32, -32, 0), Vector(32, 32, 64));

	pev->solid			= SOLID_NOT;
	pev->movetype		= MOVETYPE_STEP;
	pev->effects		= 0;
	pev->health			= 75;
	pev->yaw_speed		= 8;
	pev->sequence		= 0;
	
	pev->angles.x		= 90;
	// ResetSequenceInfo( );
}

void CTentacleMaw :: Precache( )
{
	PRECACHE_MODEL("models/maw.mdl");
}
*/