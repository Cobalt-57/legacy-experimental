//========= Copyright � 1996-2005, Valve Corporation, All rights reserved. ============//
//
// Purpose:		Player for HL1.
//
// $NoKeywords: $
//=============================================================================//

#include "cbase.h"
#include "tfc_player.h"
#include "tfc_gamerules.h"
#include "keyvalues.h"
#include "viewport_panel_names.h"
#include "client.h"
#include "team.h"
#include "weapon_tfcbase.h"
#include "tfc_client.h"
#include "tfc_mapitems.h"
#include "tfc_timer.h"
#include "tfc_engineer.h"
#include "tfc_team.h"


#define TFC_PLAYER_MODEL "models/player/pyro.mdl"


// -------------------------------------------------------------------------------- //
// Player animation event. Sent to the client when a player fires, jumps, reloads, etc..
// -------------------------------------------------------------------------------- //

class CTEPlayerAnimEvent : public CBaseTempEntity
{
public:
	DECLARE_CLASS( CTEPlayerAnimEvent, CBaseTempEntity );
	DECLARE_SERVERCLASS();

					CTEPlayerAnimEvent( const char *name ) : CBaseTempEntity( name )
					{
					}

	CNetworkHandle( CBasePlayer, m_hPlayer );
	CNetworkVar( int, m_iEvent );
	CNetworkVar( int, m_nData );
};

IMPLEMENT_SERVERCLASS_ST_NOBASE( CTEPlayerAnimEvent, DT_TEPlayerAnimEvent )
	SendPropEHandle( SENDINFO( m_hPlayer ) ),
	SendPropInt( SENDINFO( m_iEvent ), Q_log2( PLAYERANIMEVENT_COUNT ) + 1, SPROP_UNSIGNED ),
	SendPropInt( SENDINFO( m_nData ), 32 )
END_SEND_TABLE()

static CTEPlayerAnimEvent g_TEPlayerAnimEvent( "PlayerAnimEvent" );

void TE_PlayerAnimEvent( CBasePlayer *pPlayer, PlayerAnimEvent_t event, int nData )
{
	CPVSFilter filter( pPlayer->EyePosition() );
	
	// The player himself doesn't need to be sent his animation events 
	// unless cs_showanimstate wants to show them.
	/*
	if ( !ToolsEnabled() && ( cl_showanimstate.GetInt() == pPlayer->entindex() ) )
	{
		filter.RemoveRecipient( pPlayer );
	}
	*/

	g_TEPlayerAnimEvent.m_hPlayer = pPlayer;
	g_TEPlayerAnimEvent.m_iEvent = event;
	g_TEPlayerAnimEvent.m_nData = nData;
	g_TEPlayerAnimEvent.Create( filter, 0 );
}


// -------------------------------------------------------------------------------- //
// Tables.
// -------------------------------------------------------------------------------- //

LINK_ENTITY_TO_CLASS( player, CTFCPlayer );
PRECACHE_REGISTER(player);

IMPLEMENT_SERVERCLASS_ST( CTFCPlayer, DT_TFCPlayer )
	SendPropExclude( "DT_BaseAnimating", "m_flPoseParameter" ),
	SendPropExclude( "DT_BaseAnimating", "m_flPlaybackRate" ),	
	SendPropExclude( "DT_BaseAnimating", "m_nSequence" ),
	SendPropExclude( "DT_BaseEntity", "m_angRotation" ),
	SendPropExclude( "DT_BaseAnimatingOverlay", "overlay_vars" ),
	
	// cs_playeranimstate and clientside animation takes care of these on the client
	SendPropExclude( "DT_ServerAnimationData" , "m_flCycle" ),	
	SendPropExclude( "DT_AnimTimeMustBeFirst" , "m_flAnimTime" ),

	SendPropAngle( SENDINFO_VECTORELEM(m_angEyeAngles, 0), 11 ),
	SendPropAngle( SENDINFO_VECTORELEM(m_angEyeAngles, 1), 11 ),

	SendPropInt( SENDINFO( m_iRealSequence ), 9 ),

	SendPropDataTable( SENDINFO_DT( m_Shared ), &REFERENCE_SEND_TABLE( DT_TFCPlayerShared ) )
END_SEND_TABLE()


// -------------------------------------------------------------------------------- //

void cc_CreatePredictionError_f()
{
	CBaseEntity *pEnt = CBaseEntity::Instance( 1 );
	pEnt->SetAbsOrigin( pEnt->GetAbsOrigin() + Vector( 63, 0, 0 ) );
}

ConCommand cc_CreatePredictionError( "CreatePredictionError", cc_CreatePredictionError_f, "Create a prediction error", FCVAR_CHEAT );


CTFCPlayer::CTFCPlayer()
{
	m_PlayerAnimState = CreatePlayerAnimState( this );
	item_list = 0;

	UseClientSideAnimation();
	m_angEyeAngles.Init();
	m_pCurStateInfo = NULL;
	m_lifeState = LIFE_DEAD; // Start "dead".

	SetViewOffset( TFC_PLAYER_VIEW_OFFSET );

	SetContextThink( &CTFCPlayer::TFCPlayerThink, gpGlobals->curtime, "TFCPlayerThink" );
}


void CTFCPlayer::TFCPlayerThink()
{
	if ( m_pCurStateInfo && m_pCurStateInfo->pfnThink )
		(this->*m_pCurStateInfo->pfnThink)();

	SetContextThink( &CTFCPlayer::TFCPlayerThink, gpGlobals->curtime, "TFCPlayerThink" );
}


CTFCPlayer::~CTFCPlayer()
{
	m_PlayerAnimState->Release();
}


CTFCPlayer *CTFCPlayer::CreatePlayer( const char *className, edict_t *ed )
{
	CTFCPlayer::s_PlayerEdict = ed;
	return (CTFCPlayer*)CreateEntityByName( className );
}


void CTFCPlayer::PostThink()
{
	BaseClass::PostThink();

	QAngle angles = GetLocalAngles();
	angles[PITCH] = 0;
	SetLocalAngles( angles );
	
	// Store the eye angles pitch so the client can compute its animation state correctly.
	m_angEyeAngles = EyeAngles();

    m_PlayerAnimState->Update( m_angEyeAngles[YAW], m_angEyeAngles[PITCH] );
}


void CTFCPlayer::Precache()
{
	for ( int i=0; i < PC_LASTCLASS; i++ )
		PrecacheModel( GetTFCClassInfo( i )->m_pModelName );

	PrecacheScriptSound( "Player.Spawn" );
	PrecacheScriptSound( "Player.Jump" );
	PrecacheScriptSound( "TFPlayer.SaveMe1" );
	PrecacheScriptSound( "TFPlayer.SaveMe2" );

	BaseClass::Precache();
}


void CTFCPlayer::InitialSpawn( void )
{
	BaseClass::InitialSpawn();

	State_Enter( STATE_WELCOME );
}


void CTFCPlayer::Spawn()
{
	SetModel( GetTFCClassInfo( m_Shared.GetPlayerClass() )->m_pModelName );

	SetMoveType( MOVETYPE_WALK );
	RemoveSolidFlags( FSOLID_NOT_SOLID );
	m_iLegDamage = 0;

	BaseClass::Spawn();

	// Kind of lame, but CBasePlayer::Spawn resets a lot of the state that we initially want on.
	// So if we're in the welcome state, call its enter function to reset 
	if ( m_Shared.State_Get() == STATE_WELCOME )
	{
		State_Enter_WELCOME();
	}

	// If they were dead, then they're respawning. Put them in the active state.
	if ( m_Shared.State_Get() == STATE_DYING )
	{
		State_Transition( STATE_ACTIVE );
	}

	// If they're spawning into the world as fresh meat, give them items and stuff.
	if ( m_Shared.State_Get() == STATE_ACTIVE )
	{
		EmitSound( "Player.Spawn" );
		GiveDefaultItems();
	}
}


void CTFCPlayer::ForceRespawn()
{
	//TFCTODO: goldsrc tfc has a big function for this.. doing what I'm doing here may not work.
	respawn( this, false );
}

CBaseEntity    *CTFCPlayer::GiveNamedItem( const char *pszName, int iSubType )
{
	// If I already own this type don't create one
	if ( Weapon_OwnsThisType(pszName, iSubType) )
		return NULL;

	// Msg( "giving %s\n", pszName );

	EHANDLE pent;

	pent = CreateEntityByName(pszName);
	if ( pent == NULL )
	{
		Msg( "NULL Ent in GiveNamedItem!\n" );
		return NULL;
	}

	pent->SetLocalOrigin( GetLocalOrigin() );
	pent->AddSpawnFlags( SF_NORESPAWN );
    
    Vector tempVecMine = GetLocalOrigin();
    Vector tempVecPent = pent->GetLocalOrigin();
    Warning( "!!!! YO IM %s HERES THE PLAYERS LOC (%.2f %.2f %.2f) HERES MY LOC (%.2f %.2f %.2f)\n", pszName, tempVecMine.x, tempVecMine.y, tempVecMine.z, tempVecPent.x, tempVecPent.y, tempVecPent.z  );
    
 	if ( iSubType )
	{
		CBaseCombatWeapon *pWeapon = dynamic_cast<CBaseCombatWeapon*>( (CBaseEntity*)pent );
		if ( pWeapon )
		{
			pWeapon->SetSubType( iSubType );
		}
	}

	DispatchSpawn( pent );

	if ( pent != NULL && !(pent->IsMarkedForDeletion()) ) 
	{
		pent->Touch( this );
	}

	return pent;
}

void CTFCPlayer::GiveDefaultItems()
{
	Warning("This is the class I am! %d \n", m_Shared.GetPlayerClass() );

	switch( m_Shared.GetPlayerClass() )
	{
		case PC_HWGUY:
		{
			GiveNamedItem( "weapon_crowbar" );
			
			GiveNamedItem( "weapon_minigun" );
			GiveAmmo( 176, TFC_AMMO_SHELLS );
		}
		break;

		case PC_PYRO:
		{
			GiveNamedItem( "weapon_crowbar" );
		}
		break;

		case PC_ENGINEER:
		{
			GiveNamedItem( "weapon_spanner" );
			GiveNamedItem( "weapon_super_shotgun" );
			GiveAmmo( 20, TFC_AMMO_SHELLS );
		}
		break;

		case PC_SCOUT:
		{
			GiveNamedItem( "weapon_crowbar" );
			GiveNamedItem( "weapon_shotgun" );
			GiveNamedItem( "weapon_nailgun" );
			GiveAmmo( 25, TFC_AMMO_SHELLS );
			GiveAmmo( 100, TFC_AMMO_NAILS );
		}
		break;

		case PC_SNIPER:
		{
			GiveNamedItem( "weapon_crowbar" );
		}
		break;

		case PC_SOLDIER:
		{
			GiveNamedItem( "weapon_crowbar" );
		}
		break;

		case PC_DEMOMAN:
		{
			GiveNamedItem( "weapon_crowbar" );
		}
		break;

		case PC_SPY:
		{
			GiveNamedItem( "weapon_knife" );
		}
		break;

		case PC_MEDIC:
		{
			GiveNamedItem( "weapon_medikit" );
			GiveNamedItem( "weapon_super_nailgun" );
			GiveAmmo( 100, TFC_AMMO_NAILS );
		}
		break;
	}
}


void CTFCPlayer::DoAnimationEvent( PlayerAnimEvent_t event, int nData )
{
	m_PlayerAnimState->DoAnimationEvent( event, nData );
	TE_PlayerAnimEvent( this, event, nData );	// Send to any clients who can see this guy.
}

void CTFCPlayer::SetAnimation( PLAYER_ANIM playerAnim )
{
//    BaseClass::SetAnimation( playerAnim );
	if ( playerAnim == PLAYER_ATTACK1 )
	{
		DoAnimationEvent( PLAYERANIMEVENT_FIRE_GUN, 0 );
	}

	int animDesired;
	char szAnim[64];

	float speed;

	speed = GetAbsVelocity().Length2D();

	if (GetFlags() & (FL_FROZEN|FL_ATCONTROLS))
	{
		speed = 0;
		playerAnim = PLAYER_IDLE;
	}

	if ( playerAnim == PLAYER_ATTACK1 )
	{
		if ( speed > 0 )
		{
			playerAnim = PLAYER_WALK;
		}
		else
		{
			playerAnim = PLAYER_IDLE;
		}
	}

	Activity idealActivity = ACT_WALK;// TEMP!!!!!

	// This could stand to be redone. Why is playerAnim abstracted from activity? (sjb)
	if (playerAnim == PLAYER_JUMP)
	{
		idealActivity = ACT_HOP;
	}
	else if (playerAnim == PLAYER_SUPERJUMP)
	{
		idealActivity = ACT_LEAP;
	}
	else if (playerAnim == PLAYER_DIE)
	{
		if ( m_lifeState == LIFE_ALIVE )
		{
			idealActivity = ACT_DIERAGDOLL;
		}
	}
	else if (playerAnim == PLAYER_ATTACK1)
	{
		if ( GetActivity() == ACT_HOVER	|| 
			GetActivity() == ACT_SWIM		||
			GetActivity() == ACT_HOP		||
			GetActivity() == ACT_LEAP		||
			GetActivity() == ACT_DIESIMPLE )
		{
			idealActivity = GetActivity();
		}
		else
		{
			idealActivity = ACT_RANGE_ATTACK1;
		}
	}
	else if (playerAnim == PLAYER_IDLE || playerAnim == PLAYER_WALK)
	{
		if ( !( GetFlags() & FL_ONGROUND ) && (GetActivity() == ACT_HOP || GetActivity() == ACT_LEAP) )	// Still jumping
		{
			idealActivity = GetActivity();
		}
		else if ( GetWaterLevel() > 1 )
		{
			if ( speed == 0 )
				idealActivity = ACT_HOVER;
			else
				idealActivity = ACT_SWIM;
		}
		else if ( speed > 0 )
		{
			idealActivity = ACT_WALK;
		}
		else
		{
			idealActivity = ACT_IDLE;
		}
	}


	if (idealActivity == ACT_RANGE_ATTACK1)
	{
		if ( GetFlags() & FL_DUCKING )	// crouching
		{
			Q_strncpy( szAnim, "crouch_shoot_" ,sizeof(szAnim));
		}
		else
		{
			Q_strncpy( szAnim, "ref_shoot_" ,sizeof(szAnim));
		}
		Q_strncat( szAnim, m_szAnimExtension ,sizeof(szAnim), COPY_ALL_CHARACTERS );
		animDesired = LookupSequence( szAnim );
		if (animDesired == -1)
			animDesired = 0;

		if ( GetSequence() != animDesired || !SequenceLoops() )
		{
			SetCycle( 0 );
		}

		// Tracker 24588:  In single player when firing own weapon this causes eye and punchangle to jitter
		//if (!SequenceLoops())
		//{
		//	AddEffects( EF_NOINTERP );
		//}

		SetActivity( idealActivity );
		ResetSequence( animDesired );
	}
	else if (idealActivity == ACT_IDLE)
	{
		if ( GetFlags() & FL_DUCKING )
		{
			animDesired = LookupSequence( "crouch_idle" );
		}
		else
		{
			animDesired = LookupSequence( "look_idle" );
		}
		if (animDesired == -1)
			animDesired = 0;

		SetActivity( ACT_IDLE );
	}
	else if ( idealActivity == ACT_WALK )
	{
		if ( GetFlags() & FL_DUCKING )
		{
			animDesired = SelectWeightedSequence( ACT_CROUCH );
			SetActivity( ACT_CROUCH );
		}
		else
		{
			animDesired = SelectWeightedSequence( ACT_RUN );
			SetActivity( ACT_RUN );
		}
		
	}
	else
	{
		if ( GetActivity() == idealActivity)
			return;

		SetActivity( idealActivity );

		animDesired = SelectWeightedSequence( GetActivity() );

		// Already using the desired animation?
		if (GetSequence() == animDesired)
			return;

		m_iRealSequence = animDesired;
		ResetSequence( animDesired );
		SetCycle( 0 );
		return;
	}

	// Already using the desired animation?
	if (GetSequence() == animDesired)
		return;

	m_iRealSequence = animDesired;

	//Msg( "Set animation to %d\n", animDesired );
	// Reset to first frame of desired animation
	ResetSequence( animDesired );
	SetCycle( 0 );
}

void CTFCPlayer::State_Transition( TFCPlayerState newState )
{
	State_Leave();
	State_Enter( newState );
}


void CTFCPlayer::State_Enter( TFCPlayerState newState )
{
	m_Shared.m_iPlayerState = newState;
	m_pCurStateInfo = State_LookupInfo( newState );

	// Initialize the new state.
	if ( m_pCurStateInfo && m_pCurStateInfo->pfnEnterState )
		(this->*m_pCurStateInfo->pfnEnterState)();
}


void CTFCPlayer::State_Leave()
{
	if ( m_pCurStateInfo && m_pCurStateInfo->pfnLeaveState )
	{
		(this->*m_pCurStateInfo->pfnLeaveState)();
	}
}


CPlayerStateInfo* CTFCPlayer::State_LookupInfo( TFCPlayerState state )
{
	// This table MUST match the 
	static CPlayerStateInfo playerStateInfos[] =
	{
		{ STATE_ACTIVE,			"STATE_ACTIVE",			&CTFCPlayer::State_Enter_ACTIVE,		NULL,	NULL },
		{ STATE_WELCOME,		"STATE_WELCOME",		&CTFCPlayer::State_Enter_WELCOME,		NULL,	NULL },
		{ STATE_PICKINGTEAM,	"STATE_PICKINGTEAM",	&CTFCPlayer::State_Enter_PICKINGTEAM,	NULL,	NULL },
		{ STATE_PICKINGCLASS,	"STATE_PICKINGCLASS",	&CTFCPlayer::State_Enter_PICKINGCLASS,	NULL,	NULL },
		{ STATE_OBSERVER_MODE,	"STATE_OBSERVER_MODE",	&CTFCPlayer::State_Enter_OBSERVER_MODE,	NULL,	NULL },
		{ STATE_DYING,			"STATE_DYING",			&CTFCPlayer::State_Enter_DYING,			NULL,	NULL }
	};

	for ( int i=0; i < ARRAYSIZE( playerStateInfos ); i++ )
	{
		if ( playerStateInfos[i].m_iPlayerState == state )
			return &playerStateInfos[i];
	}

	return NULL;
}


void CTFCPlayer::State_Enter_WELCOME()
{
	SetMoveType( MOVETYPE_NONE );
	AddEffects( EF_NODRAW );
	AddSolidFlags( FSOLID_NOT_SOLID );
	PhysObjectSleep();
	
	// Show info panel (if it's not a simple demo map).
	KeyValues *data = new KeyValues("data");
	data->SetString( "title", "Message of the Day" ); // info panel title
	data->SetString( "type",  "3" );				  // show a file
	data->SetString( "msg",   "motd.txt" );			  // this file
	data->SetString( "cmd",   "joingame" );			  // exec this command if panel closed

	ShowViewPortPanel( "info", true, data );

	data->deleteThis();
}


void CTFCPlayer::State_Enter_PICKINGTEAM()
{
	ShowViewPortPanel( PANEL_TEAM ); // show the team menu
}


void CTFCPlayer::State_Enter_PICKINGCLASS()
{
	// go to spec mode, if dying keep deathcam
	if ( GetObserverMode() == OBS_MODE_DEATHCAM )
	{
		StartObserverMode( OBS_MODE_DEATHCAM );
	}
	else
	{
		StartObserverMode( OBS_MODE_ROAMING );
	}
	
	PhysObjectSleep();

	// show the class menu:
	ShowViewPortPanel( PANEL_CLASS );
}


void CTFCPlayer::State_Enter_OBSERVER_MODE()
{
	StartObserverMode( m_iObserverLastMode );
	PhysObjectSleep();
}


void CTFCPlayer::State_Enter_ACTIVE()
{
	SetMoveType( MOVETYPE_WALK );
	RemoveEffects( EF_NODRAW );
	RemoveSolidFlags( FSOLID_NOT_SOLID );
    m_Local.m_iHideHUD = 0;
	PhysObjectWake();
}


void CTFCPlayer::State_Enter_DYING()
{
	SetMoveType( MOVETYPE_NONE );
	AddSolidFlags( FSOLID_NOT_SOLID );
}


void CTFCPlayer::PhysObjectSleep()
{
	IPhysicsObject *pObj = VPhysicsGetObject();
	if ( pObj )
		pObj->Sleep();
}


void CTFCPlayer::PhysObjectWake()
{
	IPhysicsObject *pObj = VPhysicsGetObject();
	if ( pObj )
		pObj->Wake();
}


void CTFCPlayer::HandleCommand_JoinTeam( int iTeam )
{
/*
	//int iTeam = TEAM_RED;
	if ( stricmp( pTeamName, "auto" ) == 0 )
	{
		iTeam = RandomInt( 0, 1 ) ? TEAM_RED : TEAM_BLUE;
	}
	else if ( stricmp( pTeamName, "spectate" ) == 0 )
	{
		iTeam = TEAM_SPECTATOR;
	}
	else
	{
		for ( int i=0; i < TEAM_MAXCOUNT; i++ )
		{
			if ( stricmp( pTeamName, teamnames[i] ) == 0 )
			{
				iTeam = i;
				break;
			}
		}
	}
*/
	if ( iTeam == TEAM_SPECTATOR )
	{
		// Prevent this is the cvar is set
		if ( !mp_allowspectators.GetInt() && !IsHLTV() )
		{
			ClientPrint( this, HUD_PRINTCENTER, "#Cannot_Be_Spectator" );
			return;
		}
		
		if ( GetTeamNumber() != TEAM_UNASSIGNED && !IsDead() )
		{
			CommitSuicide();

			// add 1 to frags to balance out the 1 subtracted for killing yourself
			IncrementFragCount( 1 );
		}

		ChangeTeam( TEAM_SPECTATOR );

		// do we have fadetoblack on? (need to fade their screen back in)
		if ( mp_fadetoblack.GetInt() )
		{
			color32_s clr = { 0,0,0,0 };
			UTIL_ScreenFade( this, clr, 0.001, 0, FFADE_IN );
		}
	}
	else
	{
		ChangeTeam( iTeam );
		State_Transition( STATE_PICKINGCLASS );
	}
}


void CTFCPlayer::ChangeTeam( int iTeamNum )
{
	if ( !GetGlobalTeam( iTeamNum ) )
	{
		Warning( "CCSPlayer::ChangeTeam( %d ) - invalid team index.\n", iTeamNum );
		return;
	}

	int iOldTeam = GetTeamNumber();

	// if this is our current team, just abort
	if ( iTeamNum == iOldTeam )
		return;

	BaseClass::ChangeTeam( iTeamNum );

	if ( iTeamNum == TEAM_UNASSIGNED )
	{
		State_Transition( STATE_OBSERVER_MODE );
	}
	else if ( iTeamNum == TEAM_SPECTATOR )
	{
		State_Transition( STATE_OBSERVER_MODE );
	}
	else // active player
	{
		if ( iOldTeam == TEAM_SPECTATOR )
		{
			// If they're switching from being a spectator to ingame player
			GetIntoGame();
		}

		if ( !IsDead() && iOldTeam != TEAM_UNASSIGNED )
		{
			// Kill player if switching teams while alive
			CommitSuicide();
		}

		// Put up the class selection menu.
		State_Transition( STATE_PICKINGCLASS );
	}
}


void CTFCPlayer::HandleCommand_JoinClass( int iClass )
{
/*
	int iClass = RandomInt( 0, PC_LAST_NORMAL_CLASS );
	if ( stricmp( pClassName, "random" ) != 0 )
	{
		for ( int i=0; i < PC_LASTCLASS; i++ )
		{
			if ( stricmp( pClassName, GetTFCClassInfo( i )->m_pClassName ) == 0 )
			{
				iClass = i;
				break;
			}
		}
		if ( iClass == PC_LAST_NORMAL_CLASS )
		{
			Warning( "HandleCommand_JoinClass( %s ) - invalid class name.\n", pClassName );
		}
	}
*/
	m_Shared.SetPlayerClass( iClass );
	
	if ( !IsAlive() )
		GetIntoGame();
}


void CTFCPlayer::GetIntoGame()
{
	State_Transition( STATE_ACTIVE );
	Spawn();
}

bool CTFCPlayer::ClientCommand( const char *pcmd )
{
	//const char *pcmd = engine->Cmd_Argv(0);
	if ( FStrEq( pcmd, "joingame" ) )
	{
		// player just closed MOTD dialog
		if ( m_Shared.m_iPlayerState == STATE_WELCOME )
		{
			State_Transition( STATE_PICKINGTEAM );
		}
		return true;
	}
	else if ( FStrEq( pcmd, "jointeam" ) )
	{
		if ( engine->Cmd_Argc() >= 2 )
		{
			int iTeam = atoi( engine->Cmd_Argv(1) );
			HandleCommand_JoinTeam( iTeam );
			return true;
		}
	}
	else if ( FStrEq( pcmd, "joinclass" ) ) 
	{
		if ( engine->Cmd_Argc() < 2 )
		{
			Warning( "Player sent bad joinclass syntax\n" );
		}

		//HandleCommand_JoinClass( engine->Cmd_Argc(1) );

		int iClass = atoi( engine->Cmd_Argv(1) );
		HandleCommand_JoinClass( iClass );

		return true;
	}

	else if ( FStrEq( pcmd, "saveme" ) )
	{
		TeamFortress_SaveMe();
		return true;
	}

	return BaseClass::ClientCommand( pcmd );
}


bool CTFCPlayer::IsAlly( CBaseEntity *pEnt ) const
{
	return pEnt->GetTeamNumber() == GetTeamNumber();
}


void CTFCPlayer::TF_AddFrags( int nFrags )
{
	// TFCTODO: implement frags
}


void CTFCPlayer::ResetMenu()
{
	current_menu = 0;
}

int CTFCPlayer::GetNumFlames() const
{
	// TFCTODO: implement flames
	return 0;
}


void CTFCPlayer::SetNumFlames( int nFlames )
{
	// TFCTODO: implement frags
	Assert( 0 );
}

int CTFCPlayer::TakeHealth( float flHealth, int bitsDamageType )
{
	int bResult = false;

	// If the bit's set, ignore the monster's max health and add over it
	if ( bitsDamageType & DMG_IGNORE_MAXHEALTH )
	{
		int iDamage = DMG_TIMEBASED;
		m_bitsDamageType &= ~(bitsDamageType & ~iDamage);
		m_iHealth += flHealth;
		bResult = true;
	}
	else
	{
		bResult = BaseClass::TakeHealth( flHealth, bitsDamageType );
	}

	// Leg Healing
	if (m_iLegDamage > 0)
	{
		// Allow even at full health
		if ( GetHealth() >= (GetMaxHealth() - 5))
			m_iLegDamage = 0;
		else
			m_iLegDamage -= (GetHealth() + flHealth) / 20;
		if (m_iLegDamage < 1)
			m_iLegDamage = 0;

		TeamFortress_SetSpeed();
		bResult = true;
	}

	return bResult;
}


void CTFCPlayer::TeamFortress_SetSpeed()
{
	int playerclass = m_Shared.GetPlayerClass();
	float maxfbspeed;

	// Spectators can move while in Classic Observer mode
	if ( IsObserver() )
	{
		if ( GetObserverMode() == OBS_MODE_ROAMING )
			SetMaxSpeed( GetTFCClassInfo( PC_SCOUT )->m_flMaxSpeed );
		else
			SetMaxSpeed( 0 );

		return;
	}

	// Check for any reason why they can't move at all
	if ( (m_Shared.GetStateFlags() & TFSTATE_CANT_MOVE) || (playerclass == PC_UNDEFINED) )
	{
		SetAbsVelocity( vec3_origin );
		SetMaxSpeed( 1 );
		return;
	}

	// First, get their max class speed
	maxfbspeed = GetTFCClassInfo( playerclass )->m_flMaxSpeed;

	// 2nd, see if any GoalItems are slowing them down
	CBaseEntity *pEnt = gEntList.FindEntityByClassname( NULL, "item_tfgoal" );
	while ( pEnt )
	{
		CTFGoal *pGoal = dynamic_cast<CTFGoal*>( pEnt );
		if ( pGoal )
		{
			if ( pGoal->GetOwnerEntity() == this )
			{
				if (pGoal->goal_activation & TFGI_SLOW)
				{
					maxfbspeed = maxfbspeed / 2;
				}
				else if (pGoal->speed_reduction)
				{
					float flPercent = ((float)pGoal->speed_reduction) / 100.0;
					maxfbspeed = flPercent * maxfbspeed;
				}
			}
		}

		pEnt = gEntList.FindEntityByClassname( pEnt, "item_tfgoal" );
	}

	// 3rd, See if they're tranquilised
	if (m_Shared.GetStateFlags() & TFSTATE_TRANQUILISED)
	{
		maxfbspeed = maxfbspeed / 2;
	}

	// 4th, check for leg wounds
	if (m_iLegDamage)
	{
		if (m_iLegDamage > 6)
			m_iLegDamage = 6;

		// reduce speed by 10% per leg wound
		maxfbspeed = (maxfbspeed * ((10 - m_iLegDamage) / 10));
	}

	// 5th, if they're a sniper, and they're aiming, their speed must be 80 or less
	if (m_Shared.GetStateFlags() & TFSTATE_AIMING)
	{
		if (maxfbspeed > 80)
			maxfbspeed = 80;
	}

	// Set the speed
	SetMaxSpeed( maxfbspeed );
}


void CTFCPlayer::Event_Killed( const CTakeDamageInfo &info )
{
	DoAnimationEvent( PLAYERANIMEVENT_DIE, 4 );
	State_Transition( STATE_DYING );	// Transition into the dying state.

	// Remove all items..
	RemoveAllItems( true );

	BaseClass::Event_Killed( info );

	// Don't overflow the value for this.
	m_iHealth = 0;
}


void CTFCPlayer::ClientHearVox( const char *pSentence )
{
	//TFCTODO: implement this.
}


//=========================================================================
// Check all stats to make sure they're good for this class
void CTFCPlayer::TeamFortress_CheckClassStats()
{
	// Check armor
	if (armortype > armor_allowed)
		armortype = armor_allowed;
	
	if (ArmorValue() > GetClassInfo()->m_iMaxArmor)
		SetArmorValue( GetClassInfo()->m_iMaxArmor );

	if (ArmorValue() < 0)
		SetArmorValue( 0 );
	
	if (armortype < 0)
		armortype = 0;
	
	// Check ammo
	for ( int iAmmoType=0; iAmmoType < TFC_NUM_AMMO_TYPES; iAmmoType++ )
	{
		if ( GetAmmoCount( iAmmoType ) > GetClassInfo()->m_MaxAmmo[iAmmoType] )
			RemoveAmmo( GetAmmoCount( iAmmoType ) - GetClassInfo()->m_MaxAmmo[iAmmoType], iAmmoType );
	}

	// Check Grenades
	Assert( GetAmmoCount( TFC_AMMO_GRENADES1 ) >= 0 );
	Assert( GetAmmoCount( TFC_AMMO_GRENADES2 ) >= 0 );
	
	// Limit Nails
	if ( no_grenades_1() > g_nMaxGrenades[tp_grenades_1()] )
		RemoveAmmo( TFC_AMMO_GRENADES1, no_grenades_1() - g_nMaxGrenades[tp_grenades_1()] );

	if ( no_grenades_2() > g_nMaxGrenades[tp_grenades_2()] )
		RemoveAmmo( TFC_AMMO_GRENADES2, no_grenades_2() - g_nMaxGrenades[tp_grenades_2()] );

	// Check health
	if (GetHealth() > GetMaxHealth() && !(m_Shared.GetItemFlags() & IT_SUPERHEALTH))
		SetHealth( GetMaxHealth() );
	
	if (GetHealth() < 0)
		SetHealth( 0 );

	// Update armor picture
	m_Shared.RemoveItemFlags( IT_ARMOR1 | IT_ARMOR2 | IT_ARMOR3 );
	if (armortype >= 0.8)
		m_Shared.AddItemFlags( IT_ARMOR3 );
	else if (armortype >= 0.6)
		m_Shared.AddItemFlags( IT_ARMOR2 );
	else if (armortype >= 0.3)
		m_Shared.AddItemFlags( IT_ARMOR1 );
}


//======================================================================
// DISGUISE HANDLING
//======================================================================
// Reset spy skin and color or remove invisibility
void CTFCPlayer::Spy_RemoveDisguise()
{
	if (m_Shared.GetPlayerClass() == PC_SPY)
	{
		if ( undercover_team || undercover_skin )
			ClientPrint( this, HUD_PRINTCENTER, "#Disguise_Lost" );

		// Set their color 
		undercover_team = 0;
		undercover_skin = 0;

		immune_to_check = gpGlobals->curtime + 10;
		is_undercover = 0;

		// undisguise weapon
		TeamFortress_SetSkin();
		TeamFortress_SpyCalcName();

		Spy_ResetExternalWeaponModel();

		// get them out of any disguise menus
		if ( current_menu == MENU_SPY || current_menu == MENU_SPY_SKIN || current_menu == MENU_SPY_COLOR )
		{
			ResetMenu();
		}

		// Remove the Disguise timer
		CTimer *pTimer = Timer_FindTimer( this, TF_TIMER_DISGUISE );
		if (pTimer)
		{
			ClientPrint( this, HUD_PRINTCENTER, "#Disguise_stop" );
			Timer_Remove( pTimer );
		}
	}
}


// when the spy loses disguise reset his weapon
void CTFCPlayer::Spy_ResetExternalWeaponModel( void )
{
	// we don't show any weapon models if we're feigning
	if ( is_feigning )
		return;

#ifdef TFCTODO // spy
	pev->weaponmodel = MAKE_STRING( m_pszSavedWeaponModel );
	strcpy( m_szAnimExtention, m_szSavedAnimExtention );
	m_iCurrentAnimationState = 0; // force the current animation sequence to be recalculated
#endif
}


//=========================================================================
// Try and find the player's name who's skin and team closest fit the 
// current disguise of the spy
void CTFCPlayer::TeamFortress_SpyCalcName()
{
	CBaseEntity *last_target = undercover_target;// don't redisguise self as this person

	undercover_target = NULL;
	
	// Find a player on the team the spy is disguised as to pretend to be
	if (undercover_team != 0)
	{
		CTFCPlayer *pPlayer = NULL;

		// Loop through players
		int i;
		for ( i = 1; i <= gpGlobals->maxClients; i++ )
		{
			pPlayer = ToTFCPlayer( UTIL_PlayerByIndex( i ) );
			if ( pPlayer )
			{
				if ( pPlayer == last_target )
				{
					// choose someone else, we're trying to rid ourselves of a disguise as this one
					continue;
				}

				// First, try to find a player with same color and skins
				if (pPlayer->GetTeamNumber() == undercover_team && pPlayer->m_Shared.GetPlayerClass() == undercover_skin)
				{
					undercover_target = pPlayer;
					return;
				}
			}
		}

		for ( i = 1; i <= gpGlobals->maxClients; i++ )
		{
			pPlayer = ToTFCPlayer( UTIL_PlayerByIndex( i ) );
			
			if ( pPlayer )
			{
				if (pPlayer->GetTeamNumber() == undercover_team)
				{
					undercover_target = pPlayer;
					return;
				}
			}
		}
	}
}


//=========================================================================
// Set the skin of a player based on his/her class
void CTFCPlayer::TeamFortress_SetSkin()
{
	immune_to_check = gpGlobals->curtime + 10;

	// Find out whether we should show our actual class or a disguised class
	int iClassToUse = m_Shared.GetPlayerClass();
	if (iClassToUse == PC_SPY && undercover_skin != 0)
		iClassToUse = undercover_skin;
	
	int iTeamToUse = GetTeamNumber();
	if (m_Shared.GetPlayerClass() == PC_SPY && undercover_team != 0)
		iTeamToUse = undercover_team;

// TFCTODO: handle replacement_model here.

	SetModel( GetTFCClassInfo( iClassToUse )->m_pModelName );

	// Skins in the models should be setup using the team IDs in tfc_shareddefs.h, subtracting 1
	// so they're 0-based.
	m_nSkin = iTeamToUse - 1;

	if ( FBitSet(GetFlags(), FL_DUCKING) ) 
		UTIL_SetSize(this, VEC_DUCK_HULL_MIN, VEC_DUCK_HULL_MAX);
	else
		UTIL_SetSize(this, VEC_HULL_MIN, VEC_HULL_MAX);
}



//=========================================================================
// Displays the state of the items specified by the Goal passed in
void CTFCPlayer::DisplayLocalItemStatus( CTFGoal *pGoal )
{
	for (int i = 0; i < 4; i++)
	{
		if (pGoal->display_item_status[i] != 0)
		{
			CTFGoalItem *pItem = Finditem(pGoal->display_item_status[i]);
			if (pItem)
				DisplayItemStatus(pGoal, this, pItem);
			else
				ClientPrint( this, HUD_PRINTTALK, "#Item_missing" );
		}
	}
}


//=========================================================================
// Removes all the Engineer's buildings
void CTFCPlayer::Engineer_RemoveBuildings()
{
	// If the player's building already, stop
	if (is_building == 1)
	{
		m_Shared.RemoveStateFlags( TFSTATE_CANT_MOVE );
		TeamFortress_SetSpeed();

		// Remove the timer
		CTimer *pTimer = Timer_FindTimer( this, TF_TIMER_BUILD );
		if (pTimer)
			Timer_Remove(pTimer);
		
		// Remove the building
		UTIL_Remove( building );
		building = NULL;
		is_building = 0;

		// Stop Build Sound
		StopSound( "Engineer.Building" );
		//STOP_SOUND( ENT(pev), CHAN_STATIC, "weapons/building.wav" );

		if ( GetActiveWeapon() )
			GetActiveWeapon()->Deploy();
	}

	DestroyBuilding(this, "building_dispenser");
	DestroyBuilding(this, "building_sentrygun");
	DestroyTeleporter(this, BUILD_TELEPORTER_ENTRY);
	DestroyTeleporter(this, BUILD_TELEPORTER_EXIT);
}


//=========================================================================
// Removes all grenades that persist for a period of time from the world
void CTFCPlayer::TeamFortress_RemoveLiveGrenades( void )
{
	RemoveOwnedEnt( "tf_weapon_napalmgrenade" );
	RemoveOwnedEnt( "tf_weapon_nailgrenade" );
	RemoveOwnedEnt( "tf_weapon_gasgrenade" );
	RemoveOwnedEnt( "tf_weapon_caltrop" );
}


//=========================================================================
// Removes all rockets the player has fired into the world
// (this prevents a team kill cheat where players would fire rockets 
// then change teams to kill their own team)
void CTFCPlayer::TeamFortress_RemoveRockets( void )
{
	RemoveOwnedEnt( "tf_rpg_rocket" );
	RemoveOwnedEnt( "tf_ic_rocket" );
}

// removes the player's pipebombs with no explosions
void CTFCPlayer::RemovePipebombs( void )
{
	CBaseEntity *pEnt = gEntList.FindEntityByClassname( NULL, "tf_gl_pipebomb" );
	while ( pEnt )
	{
		CTFCPlayer *pOwner = ToTFCPlayer( pEnt->GetOwnerEntity() );
		if ( pOwner == this )
		{
			pOwner->m_iPipebombCount--;
			pEnt->AddFlag( FL_KILLME );
		}

		pEnt = gEntList.FindEntityByClassname( pEnt, "tf_gl_pipebomb" );
	}
}


//=========================================================================
// Stops the setting of the detpack
void CTFCPlayer::TeamFortress_DetpackStop( void )
{
	CTimer *pTimer = Timer_FindTimer( this, TF_TIMER_DETPACKSET );

	if (!pTimer)
	    return;

    ClientPrint( this, HUD_PRINTNOTIFY, "#Detpack_retrieve" );

	// Return the detpack
	GiveAmmo( 1, TFC_AMMO_DETPACK );
	Timer_Remove(pTimer);

	// Release player
	m_Shared.RemoveStateFlags( TFSTATE_CANT_MOVE );
	is_detpacking = 0;
	TeamFortress_SetSpeed();

	// Return their weapon
	if ( GetActiveWeapon() )
		GetActiveWeapon()->Deploy();
}


//=========================================================================
// Removes any detpacks the player may have set
BOOL CTFCPlayer::TeamFortress_RemoveDetpacks( void )
{
	// Remove all detpacks owned by the player
	CBaseEntity *pEnt = gEntList.FindEntityByClassname( NULL, "detpack" );
	while ( pEnt )
	{
		// if the player owns this detpack, remove it
		if ( pEnt->GetOwnerEntity() == this )
		{
			UTIL_Remove( pEnt );
			return TRUE;
		}

		pEnt = gEntList.FindEntityByClassname( pEnt, "detpack" );
	}

	return FALSE;
}


//Convenience method ~blue24.
BOOL CTFCPlayer::IsPlayerClass(int classToCompareTo){
	return (m_Shared.GetPlayerClass() == classToCompareTo);
}


//=========================================================================
// Discard Command. Drops all ammo useless to the player's class.
void CTFCPlayer::TeamFortress_Discard ()
{ 
	// I NEED CONVERTED!!!
	/*
	// Create a backpack
	newmis = spawn();
	
	// Fill it with ammo based upon the player's class
	if (self.playerclass == #PC_SCOUT)
	{
		// Doesn't need rockets
		newmis.ammo_rockets = self.ammo_rockets;
	}
	else if (self.playerclass == #PC_SNIPER)
	{
		// Doesn't need rockets or cells
		newmis.ammo_rockets = self.ammo_rockets;
		newmis.ammo_cells = self.ammo_cells;
	}
	else if (self.playerclass == #PC_SOLDIER)
	{
		// Doesn't need cells or nails
		newmis.ammo_cells = self.ammo_cells;
		newmis.ammo_nails = self.ammo_nails;
	}
	else if (self.playerclass == #PC_DEMOMAN)
	{
		// Doesn't need cells or nails
		newmis.ammo_cells = self.ammo_cells;
		newmis.ammo_nails = self.ammo_nails;
	}
	else if (self.playerclass == #PC_MEDIC)
	{
		// Doesn't need rockets or cells
		newmis.ammo_rockets = self.ammo_rockets;
		newmis.ammo_cells = self.ammo_cells;
	}
	else if (self.playerclass == #PC_HVYWEAP)
	{
		// Doesn't need rockets or nails
		newmis.ammo_rockets = self.ammo_rockets;
		newmis.ammo_nails = self.ammo_nails;
	}
	else if (self.playerclass == #PC_PYRO)
	{
		// Doesn't need nails
		newmis.ammo_nails = self.ammo_nails;
	}
	else if (self.playerclass == #PC_SPY)
	{
		// Doesn't need rockets
		newmis.ammo_rockets = self.ammo_rockets;
	}
	else if (self.playerclass == #PC_ENGINEER)
	{
		// Doesn't need rockets 
		newmis.ammo_rockets = self.ammo_rockets;
	}

	// If there's nothing in the backpack, remove it and return
	if (!(newmis.ammo_shells + newmis.ammo_nails + newmis.ammo_rockets + newmis.ammo_cells))
	{
		dremove(newmis);
		return;
	}

	// Remove the ammo from the player
	if (newmis.ammo_shells)
		self.ammo_shells = 0;
	if (newmis.ammo_nails)
		self.ammo_nails = 0;
	if (newmis.ammo_rockets)
		self.ammo_rockets = 0;
	if (newmis.ammo_cells)
		self.ammo_cells = 0;
	W_SetCurrentAmmo();

	sound(self, #CHAN_ITEM, "weapons/lock4.wav", 1, #ATTN_NORM);

	// The backpack is treated as an ammobox, so people can't crash svrs
	// by making too many of them.
	if (self.team_no != 0)
	{
		increment_team_ammoboxes(self.team_no);
		if (num_team_ammoboxes(self.team_no) > (#MAX_WORLD_AMMOBOXES / number_of_teams))
			RemoveOldAmmobox(self.team_no);
	}
	else 
	{
		num_world_ammoboxes = num_world_ammoboxes + 1;
		if (num_world_ammoboxes > #MAX_WORLD_AMMOBOXES)
			RemoveOldAmmobox(0);
	}

	// Throw the backpack
	newmis.enemy = self;
	newmis.health = time;
	newmis.weapon = 0;
	newmis.movetype = #MOVETYPE_TOSS;
	newmis.solid = #SOLID_TRIGGER;
	newmis.classname = "ammobox";
	newmis.team_no = self.team_no;
	makevectors (self.v_angle);
	if (self.v_angle_x)
	{
		newmis.velocity = v_forward*400 + v_up * 200;
	}
	else
	{
		newmis.velocity = aim(self, 10000);
		newmis.velocity = newmis.velocity * 400;
		newmis.velocity_z = 200;
	}
	newmis.avelocity = '0 300 0';
	setsize (newmis, '0 0 0', '0 0 0');		
	setorigin (newmis, self.origin);
	newmis.nextthink = time + 30;	// remove after 30 seconds
	newmis.think = SUB_Remove;
	newmis.touch = TeamFortress_AmmoboxTouch;
	setmodel (newmis, "progs/backpack.mdl");
	*/
}


//=========================================================================
// Shows any medics/engineers on your team that you need help.
// Spy's see all requests.
void CTFCPlayer::TeamFortress_SaveMe( void )
{
	//local entity te, tl;

	if ( last_saveme_sound < gpGlobals->curtime )
	{
		float	flRndSound;//sound randomizer

		flRndSound = random->RandomFloat( 0 , 1 ); 

		if ( flRndSound < 0.8 )
		{
			CPASAttenuationFilter filter( this );
			EmitSound( filter, entindex(), "TFPlayer.SaveMe1" ); // MEDIC!
		}
		else
		{
			CPASAttenuationFilter filter( this );
			EmitSound( filter, entindex(), "TFPlayer.SaveMe2" ); // Excuse me...
		}
		last_saveme_sound = gpGlobals->curtime + 4;
	}
	
	
	
	//needs converted
	// - IN PROGRESS, TESTING. ~blue24.
	Msg("TeamFortress_SaveMe FLAG1\n");

	CBaseEntity *tempPlayer = gEntList.FindEntityByClassname(NULL, "player");
	while (tempPlayer)
	{
		Msg("TeamFortress_SaveMe FLAG2\n");
		if (this == tempPlayer || IsPlayerClass( PC_MEDIC ) || IsPlayerClass( PC_ENGINEER ) || IsPlayerClass( PC_SPY ) )
		{
			Msg("TeamFortress_SaveMe FLAG3\n");
			//TODO - unsure what comparing a team number to 0 like in the original quake source meant. Maybe just not-spectator? part of
			// any normal team?
			if ((tempPlayer->GetTeamNumber() == this->GetTeamNumber() && this->GetTeamNumber() > TEAM_SPECTATOR) || (IsPlayerClass( PC_SPY )))
			{
				Msg("TeamFortress_SaveMe FLAG4!!!\n");
				// If the other teammate is visible, show them I need help

				//if (visible(te))
				//if(UTIL_FindClientInVisibilityPVS(tempPlayer->edict()))
				{
					/*
					msg_entity = te;
					tl = spawn();
					tl.origin = self.origin;
					tl.origin_z = tl.origin_z + 32;
					WriteByte (#MSG_ONE, #SVC_TEMPENTITY);
					WriteByte (#MSG_ONE, #TE_LIGHTNING3);
					WriteEntity (#MSG_ONE, tl);
					WriteCoord (#MSG_ONE, tl.origin_x);
					WriteCoord (#MSG_ONE, tl.origin_y);
					WriteCoord (#MSG_ONE, tl.origin_z + 24);
					WriteCoord (#MSG_ONE, self.origin_x);
					WriteCoord (#MSG_ONE, self.origin_y);
					WriteCoord (#MSG_ONE, self.origin_z);
					dremove(tl);
					*/
					
					CBroadcastRecipientFilter filterT;
					te->DynamicLight( filterT,
						0.0,                                    //delay
						&GetAbsOrigin(),							//origin
						0,		//r
						255,	//g
						0,		//b
						2,		//expoent (?)
						180,	//radius
						3.0f,	//time
						0.6f	//decay
						);

				}


			}
		}

		tempPlayer = gEntList.FindEntityByClassname(tempPlayer, "player");
	}



/*
	te = find(world, classname, "player");
	while (te)
	{
		if (self == te || IsPlayerClass( PC_MEDIC ) || IsPlayerClass( PC_ENGINEER ) || IsPlayerClass( PC_SPY ) )
		{
			if ((te.team_no == self.team_no && self.team_no != 0) || (IsPlayerClass( PC_SPY )))
			{
				// If the other teammate is visible, show them I need help
				if (visible(te))
				{
					msg_entity = te;
					tl = spawn();
					tl.origin = self.origin;
					tl.origin_z = tl.origin_z + 32;
					WriteByte (#MSG_ONE, #SVC_TEMPENTITY);
					WriteByte (#MSG_ONE, #TE_LIGHTNING3);
					WriteEntity (#MSG_ONE, tl);
					WriteCoord (#MSG_ONE, tl.origin_x);
					WriteCoord (#MSG_ONE, tl.origin_y);
					WriteCoord (#MSG_ONE, tl.origin_z + 24);
					WriteCoord (#MSG_ONE, self.origin_x);
					WriteCoord (#MSG_ONE, self.origin_y);
					WriteCoord (#MSG_ONE, self.origin_z);
					dremove(tl);
				}
			}
		}

		te = find(te, classname, "player");
	}
*/
}

//=========================================================================
// Remove all of an ent owned by this player
void CTFCPlayer::RemoveOwnedEnt( char *pEntName )
{
	CBaseEntity *pEnt = gEntList.FindEntityByClassname( NULL, pEntName );
	while ( pEnt )
	{
		// if the player owns this entity, remove it
		if ( pEnt->GetOwnerEntity() == this )
			pEnt->AddFlag( FL_KILLME );
		
		pEnt = gEntList.FindEntityByClassname( pEnt, pEntName );
	}
}

ConVar tfc_new_pull( "tfc_new_pull", "1" );
void PlayerPickupObject( CBasePlayer *pPlayer, CBaseEntity *pObject )
{
	if( tfc_new_pull.GetBool() )
	{
		//er.. lolwhut.  Best to name the newly casted pPlayer (left-most one) to something new,
		// like pPlayerTF.  Leave ambiguity to the computer at thine own risk. Or some deep shit like that.
		//CTFCPlayer	*pPlayer = dynamic_cast<CTFCPlayer*>(pPlayer);
		//Don't feel like adding this in yet, too woven in to the Player and CPlayer of HLS - Theuaredead`
		/*
		if( pPlayer && !pPlayer->IsPullingObject() )
		{
			pPlayer->StartPullingObject(pObject);
		}
		*/
	}
}


// HACKY SHIT
CBaseEntity	*g_pLastSpawnTFC = NULL;


//WE'RE HARDCODING THE INFO_ NAMES LIKE A MOTHERFUCKER
//They were in the only place that ever calls here though so whatever
//NOPE, this is fine.  We'll handle it in Ent... below.
CBaseEntity *FindPlayerStart_TFC(CTFCPlayer* arg_this, const char *pszClassName)
//CBaseEntity *FindPlayerStart_TFC(CTFCPlayer* arg_this)
{
	#define SF_PLAYER_START_MASTER	1
	

	CBaseEntity *pStart = gEntList.FindEntityByClassname(NULL, pszClassName);
	CBaseEntity *pStartFirst = pStart;
	while (pStart != NULL)
	{
		//if (pStart->HasSpawnFlags(SF_PLAYER_START_MASTER))
		if(arg_this->GetTeamNumber() == pStart->GetTeamNumber())
		{
			return pStart;
		}

		pStart = gEntList.FindEntityByClassname(pStart, pszClassName);
	}
	
	return pStartFirst;
}




CBaseEntity *CTFCPlayer::EntSelectSpawnPoint()
{
	CBaseEntity *pSpot;
	edict_t		*player;

	player = edict();

// choose a info_player_deathmatch point
	if (g_pGameRules->IsCoOp())
	{
		pSpot = gEntList.FindEntityByClassname( g_pLastSpawnTFC, "info_player_coop");
		if ( pSpot )
			goto ReturnSpot;
		pSpot = gEntList.FindEntityByClassname( g_pLastSpawnTFC, "info_player_start");
		if ( pSpot ) 
			goto ReturnSpot;
	}
	else if ( g_pGameRules->IsDeathmatch() )
	{
		pSpot = g_pLastSpawnTFC;
		// Randomize the start spot
		for ( int i = random->RandomInt(1,5); i > 0; i-- )
			pSpot = gEntList.FindEntityByClassname( pSpot, "info_player_deathmatch" );
		if ( !pSpot )  // skip over the null point
			pSpot = gEntList.FindEntityByClassname( pSpot, "info_player_deathmatch" );

		CBaseEntity *pFirstSpot = pSpot;

		do 
		{
			if ( pSpot )
			{
				// check if pSpot is valid
				if ( g_pGameRules->IsSpawnPointValid( pSpot, this ) )
				{
					if ( pSpot->GetLocalOrigin() == vec3_origin )
					{
						pSpot = gEntList.FindEntityByClassname( pSpot, "info_player_deathmatch" );
						continue;
					}

					// if so, go to pSpot
					goto ReturnSpot;
				}
			}
			// increment pSpot
			pSpot = gEntList.FindEntityByClassname( pSpot, "info_player_deathmatch" );
		} while ( pSpot != pFirstSpot ); // loop if we're not back to the start

		// we haven't found a place to spawn yet,  so kill any guy at the first spawn point and spawn there
		if ( pSpot )
		{
			CBaseEntity *ent = NULL;
			for ( CEntitySphereQuery sphere( pSpot->GetAbsOrigin(), 128 ); (ent = sphere.GetCurrentEntity()) != NULL; sphere.NextEntity() )
			{
				// if ent is a client, kill em (unless they are ourselves)
				if ( ent->IsPlayer() && !(ent->edict() == player) )
					ent->TakeDamage( CTakeDamageInfo( GetContainingEntity(INDEXENT(0)), GetContainingEntity(INDEXENT(0)), 300, DMG_GENERIC ) );
			}
			goto ReturnSpot;
		}
	}

	// If startspot is set, (re)spawn there.
	if ( !gpGlobals->startspot || !strlen(STRING(gpGlobals->startspot)))
	{
		
		//pSpot = FindPlayerStart( "info_player_start" );
		
		//NEW LOGIC
		///////////////////////////////////////////////////////////////////////
		const char* pszClassName;
		
		if(this->GetTeamNumber() > TEAM_SPECTATOR){
			//I'm a real player!
			pszClassName = "info_player_teamspawn";
		}else{
			//awwww
			pszClassName = "info_player_start";
		}
		
		pSpot = FindPlayerStart_TFC( this, pszClassName );
		
		if(!pSpot){
			//Still failed? use the fallback
			pSpot = FindPlayerStart_TFC(this, "info_player_deathmatch");
		}
		///////////////////////////////////////////////////////////////////////
		
		
		
		
		
		if ( pSpot )
			goto ReturnSpot;
	}
	else
	{
		pSpot = gEntList.FindEntityByName( NULL, gpGlobals->startspot );
		if ( pSpot )
			goto ReturnSpot;
	}

ReturnSpot:
	if ( !pSpot  )
	{
		Warning( "PutClientInServer: no info_player_start on level\n");
		return CBaseEntity::Instance( INDEXENT( 0 ) );
	}

	g_pLastSpawnTFC = pSpot;
	return pSpot;
}