//========= Copyright � 1996-2002, Valve LLC, All rights reserved. ============
//
// Purpose: 
//
// $NoKeywords: $
//=============================================================================

#ifndef COUNTERSTRIKEVIEWPORT_H
#define COUNTERSTRIKEVIEWPORT_H

// viewport interface for the rest of the dll
#include "vgui/ICstrikeViewPortMsgs.h"

// TAGGG - I'm sorry.  what.       why.
// well we added the file back in. I think.
// maybe not.  I don't get this.
//#include <game_controls/vgui_TeamFortressViewport.h>

#include "cs_shareddefs.h"

// TAGGG - how about this instead
//#include "basemodviewport.h"
#include "game_controls/baseviewport.h"


using namespace vgui;

namespace vgui 
{
	class Panel;
}

class CCSTeamMenu;
class CCSClassMenu;
class CCSSpectatorGUI;
class CCSClientScoreBoard;
class CBuyMenu;
class CCSClientScoreBoardDialog;



//TAGGG - why the hell not
extern int GetMaxPlayers();


//==============================================================================

//TAGGG - is this ok
//        BUT keep in mind. in the "src2003" files, there actually is a 
//        "BaseModViewport.h / .cpp" pair in client.
//        As for whether it is significant to really go the full "CbaseModViewport" route
//        or just use the "CBaseViewport" the rest of all these files seem to be using...
//        yea I think I answered my own question. But just keep that in mind as an option.
//class CounterStrikeViewport : public CBaseModViewport, public ICSViewPortMsgs
class CounterStrikeViewport : public CBaseViewport, public ICSViewPortMsgs
{

private:
	//TAGGG - ditto
	//typedef CBaseModViewport BaseClass;
	typedef CBaseViewport BaseClass;

public:
	CounterStrikeViewport();
	~CounterStrikeViewport();

	//TAGGG - see note in the cpp file
	//virtual void Start( IGameUIFuncs *pGameUIFuncs, IGameEventManager * gameeventmanager );
	virtual void Start( IGameUIFuncs *pGameUIFuncs, IGameEventManager2 * gameeventmanager );

	virtual void SetParent(vgui::VPANEL parent);
	virtual void Enable();
	virtual void Disable();
	virtual void OnThink();
	virtual void ApplySchemeSettings( vgui::IScheme *pScheme );
	virtual void HideAllVGUIMenu( void );   
	
	virtual int	 KeyInput( int down, int keynum, const char *pszCurrentBinding );

	virtual void HideVGUIMenu( int iMenu );
	virtual void OnLevelChange(const char * mapname);
	virtual void ReloadScheme();

	AnimationController * GetAnimationController() { return m_pAnimController; }

	virtual void UpdateSpectatorPanel();
	// override this message
	virtual int MsgFunc_TeamInfo( const char *pszName, int iSize, void *pbuf );
	virtual int MsgFunc_ScoreInfo( const char *pszName, int iSize, void *pbuf );
	virtual int MsgFunc_TeamScore( const char *pszName, int iSize, void *pbuf );

private:

	virtual bool IsVGUIMenuActive( int iMenu );
	virtual void DisplayVGUIMenu( int iMenu );


	virtual void OnTick(); // per frame think function

		

	// main panels
	CCSTeamMenu *m_pTeamMenu;
	CCSClassMenu *m_pClassMenu;
	CBuyMenu	*m_pBuyMenu;
	CCSSpectatorGUI *m_pPrivateSpectatorGUI;
	CCSClientScoreBoardDialog *m_pPrivateClientScoreBoard;

	int			m_iDeadSpecMode; // used to update the m_playermenu for spectators

	HCursor					m_CursorNone;
	AnimationController*	m_pAnimController;
};


#endif // COUNTERSTRIKEVIEWPORT_H
