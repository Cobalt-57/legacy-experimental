//========= Copyright © 1996-2001, Valve LLC, All rights reserved. ============
//
// Purpose: 
//
//=============================================================================

//TAGGG - NEW FILE. Modded clone of weapon_hegrenade.cpp, which looks
//        to be nearly identical to weapon_flashbang or whatever anyways.  so either's a fine basis.

#include "cbase.h"
#include "weapon_csbase.h"
#include "gamerules.h"
#include "npcevent.h"
#include "engine/IEngineSound.h"
#include "weapon_basecsgrenade.h"


#ifdef CLIENT_DLL
	
	#define CSmokeGrenade C_SmokeGrenade

#else

	#include "cs_player.h"
	#include "items.h"
	#include "smokegrenade_projectile.h"

#endif


#define GRENADE_TIMER	3.0f //Seconds





class CSmokeGrenade : public CBaseCSGrenade
{
public:
	DECLARE_CLASS( CSmokeGrenade, CBaseCSGrenade );
	DECLARE_NETWORKCLASS();
	DECLARE_PREDICTABLE();

	CSmokeGrenade() {}

	//TAGGG - not supporting this weapon constants system.
	//That is in cs_weapon_parse.h in CS2007 (CSWeaponID)
	//virtual CSWeaponID GetWeaponID( void ) const		{ return WEAPON_SMOKEGRENADE; }

#ifdef CLIENT_DLL

#else
	DECLARE_DATADESC();

	virtual void EmitGrenade( Vector vecSrc, QAngle vecAngles, Vector vecVel, AngularImpulse angImpulse, CBasePlayer *pPlayer );
	
#endif

	CSmokeGrenade( const CSmokeGrenade & ) {}
};



IMPLEMENT_NETWORKCLASS_ALIASED( SmokeGrenade, DT_SmokeGrenade )

BEGIN_NETWORK_TABLE(CSmokeGrenade, DT_SmokeGrenade)
END_NETWORK_TABLE()

BEGIN_PREDICTION_DATA( CSmokeGrenade )
END_PREDICTION_DATA()

LINK_ENTITY_TO_CLASS( weapon_smokegrenade, CSmokeGrenade );
PRECACHE_WEAPON_REGISTER( weapon_smokegrenade );


#ifndef CLIENT_DLL

	BEGIN_DATADESC( CSmokeGrenade )
	END_DATADESC()

	void CSmokeGrenade::EmitGrenade( Vector vecSrc, QAngle vecAngles, Vector vecVel, AngularImpulse angImpulse, CBasePlayer *pPlayer )
	{
		//REMEMBAH - NO TIMAH.
		CSmokeGrenadeProjectile::Create( vecSrc, vecAngles, vecVel, angImpulse, pPlayer );
	}
	
#endif






