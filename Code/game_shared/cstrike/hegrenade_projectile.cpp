//====== Copyright � 1996-2003, Valve Corporation, All rights reserved. =======
//
// Purpose: 
//
//=============================================================================

#include "cbase.h"
#include "hegrenade_projectile.h"


//TAGGG - edit
//#define GRENADE_MODEL "models/Weapons/w_grenade.mdl"
#define GRENADE_MODEL "models/weapons/w_hegrenade.mdl"


//TAGGG NOTE - eh??  Source2007 says to use this instead. I don't know.
//#define GRENADE_MODEL "models/Weapons/w_eq_fraggrenade_thrown.mdl"



LINK_ENTITY_TO_CLASS( hegrenade_projectile, CHEGrenadeProjectile );
PRECACHE_WEAPON_REGISTER( hegrenade_projectile );


CHEGrenadeProjectile* CHEGrenadeProjectile::Create( 
	const Vector &position, 
	const QAngle &angles, 
	const Vector &velocity, 
	const AngularImpulse &angVelocity, 
	CBaseCombatCharacter *pOwner, 
	float timer )
{

	//TAGGG - w-... why was this ever OK for clientside...    w- ....   whyyyy.

#ifndef CLIENT_DLL
	
	CHEGrenadeProjectile *pGrenade = (CHEGrenadeProjectile*)CBaseEntity::Create( "hegrenade_projectile", position, angles, pOwner );
	
	// Set the timer for 1 second less than requested. We're going to issue a SOUND_DANGER
	// one second before detonation.


	pGrenade->SetMoveType( MOVETYPE_FLYGRAVITY );
	pGrenade->SetMoveCollide( MOVECOLLIDE_FLY_BOUNCE );
	pGrenade->SetTimer( timer - 1.5 );
	pGrenade->SetAbsVelocity( velocity );
	//Was SetOwner - Theuaredead`
	pGrenade->SetOwnerEntity( pOwner );
	pGrenade->m_flDamage = 100;
	pGrenade->ChangeTeam( pOwner->GetTeamNumber() );

	//ALSO IMPORTANT, apparently.
	pGrenade->SetThrower(pOwner);

	return pGrenade;

#else
	return NULL;  //placeholder
#endif	

}


void CHEGrenadeProjectile::SetTimer( float timer )
{
	SetThink( &CHEGrenadeProjectile::PreDetonate );
	SetNextThink( gpGlobals->curtime + timer );
}


void CHEGrenadeProjectile::Spawn()
{
	SetModel( GRENADE_MODEL );

	BaseClass::Spawn();
}


void CHEGrenadeProjectile::Precache()
{
	//TAGGG - unFUCK THIS.
	/*
	engine->PrecacheModel( GRENADE_MODEL );
	*/
	//new line.?
	PrecacheModel( GRENADE_MODEL );

	BaseClass::Precache();
}

