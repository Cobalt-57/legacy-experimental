//========= Copyright � 1996-2005, Valve Corporation, All rights reserved. ============//
//
// Purpose: 
//
//=============================================================================//

//TAGGG - file moved from CS2007, absent from CS2003

#include "cbase.h"
#include "weapon_csbase.h"

//TAGGG - nope, didn't have this file or expected method as of CS2003 apparently.
//#include "fx_cs_shared.h"


#if defined( CLIENT_DLL )

	#define CWeaponXM1014 C_WeaponXM1014
	#include "c_cs_player.h"

#else

	#include "cs_player.h"
	//#include "te_shotgun_shot.h"

#endif


class CWeaponXM1014 : public CWeaponCSBase
{
public:
	DECLARE_CLASS( CWeaponXM1014, CWeaponCSBase );
	DECLARE_NETWORKCLASS(); 
	DECLARE_PREDICTABLE();
	
	CWeaponXM1014();

	virtual void Spawn();
	virtual void PrimaryAttack();
	virtual bool Reload();
	virtual void WeaponIdle();

	//TAGGG - we not be doin' that mun
	//virtual CSWeaponID GetWeaponID( void ) const		{ return WEAPON_XM1014; }


private:

	CWeaponXM1014( const CWeaponXM1014 & );

	float m_flPumpTime;
	CNetworkVar( int, m_fInSpecialReload );

};

IMPLEMENT_NETWORKCLASS_ALIASED( WeaponXM1014, DT_WeaponXM1014 )

BEGIN_NETWORK_TABLE( CWeaponXM1014, DT_WeaponXM1014 )
	#ifdef CLIENT_DLL
		RecvPropInt( RECVINFO( m_fInSpecialReload ) )
	#else
		SendPropInt( SENDINFO( m_fInSpecialReload ), 2, SPROP_UNSIGNED )
	#endif
END_NETWORK_TABLE()

BEGIN_PREDICTION_DATA( CWeaponXM1014 )
END_PREDICTION_DATA()

LINK_ENTITY_TO_CLASS( weapon_xm1014, CWeaponXM1014 );
PRECACHE_WEAPON_REGISTER( weapon_xm1014 );



CWeaponXM1014::CWeaponXM1014()
{
	m_flPumpTime = 0;
}

void CWeaponXM1014::Spawn()
{
	//m_iDefaultAmmo = M3_DEFAULT_GIVE;
	//FallInit();// get ready to fall
	BaseClass::Spawn();
}


void CWeaponXM1014::PrimaryAttack()
{
	CCSPlayer *pPlayer = GetPlayerOwner();
	if ( !pPlayer )
		return;

	// don't fire underwater
	if (pPlayer->GetWaterLevel() == 3)
	{
		PlayEmptySound( );
		m_flNextPrimaryAttack = gpGlobals->curtime + 0.15;
		return;
	}

	if (m_iClip1 <= 0)
	{
		Reload();
		
		if (m_iClip1 == 0)
		{
			PlayEmptySound( );
			m_flNextPrimaryAttack = gpGlobals->curtime + 0.25;
		}

		return;
	}

	 SendWeaponAnim( ACT_VM_PRIMARYATTACK );
	 
	//pPlayer->m_iWeaponVolume = LOUD_GUN_VOLUME;
	//pPlayer->m_iWeaponFlash = BRIGHT_GUN_FLASH;

	m_iClip1--;
	pPlayer->DoMuzzleFlash();

	// player "shoot" animation
	pPlayer->SetAnimation( PLAYER_ATTACK1 );
	
	
	// Dispatch the FX right away with full accuracy.
	//TAGGG - apparently CS2003 does not do it this way.
	// Example below based off of weapon_p228.cpp
	/*
	FX_FireBullets( 
		pPlayer->entindex(),
		pPlayer->Weapon_ShootPosition(), 
		pPlayer->EyeAngles() + 2.0f * pPlayer->GetPunchAngle(), 
		GetWeaponID(),
		Primary_Mode,
		CBaseEntity::GetPredictionRandomSeed() & 255, // wrap it for network traffic so it's the same between client and server
		0.0725 // flSpread
		);
	*/

	/*
	void FX_FireBullets( 
	int	iPlayerIndex,
	const Vector &vOrigin,
	const QAngle &vAngles,
	int	iWeaponID,
	int	iMode,
	int iSeed,
	float flSpread
	)
	*/
	
	/*
	Vector CCSPlayer::FireBullets3( 
	Vector vecSrc, 
	const QAngle &shootAngles, 
	float vecSpread, 
	float flDistance, 
	int iPenetration, 
	int iBulletType, 
	int iDamage, 
	float flRangeModifier, 
	CBaseEntity *pevAttacker )
	*/


	//TAGGG - replacement, easy to understand.
	float flSpread = 0.04000;
	float flDistance = 3000;  //aka RANGE... I think?
	int iPenetration = 1;  //what?
	int iDamage = 22;
	float flRangeModifier = 0.70;
	int iBullet_Target = 6;


	for ( int iBullet=0; iBullet < iBullet_Target; iBullet++ )
	{
		pPlayer->FireBullets3( 
			pPlayer->Weapon_ShootPosition(),
			//TAGGG - old way multiplied punchangle by 2, so do it here too?
			pPlayer->EyeAngles() + 2.0f * pPlayer->GetPunchAngle(),
			flSpread, 
			flDistance, 
			iPenetration, 
			GetPrimaryAmmoType(), 
			iDamage, 
			flRangeModifier, 
			pPlayer
		);
	}







	if (!m_iClip1 && pPlayer->GetAmmoCount( m_iPrimaryAmmoType ) <= 0)
	{
		// HEV suit - indicate out of ammo condition
		pPlayer->SetSuitUpdate("!HEV_AMO0", false, 0);
	}

	if (m_iClip1 != 0)
		m_flPumpTime = gpGlobals->curtime + 0.5;

	m_flNextPrimaryAttack = gpGlobals->curtime + 0.25;
	m_flNextSecondaryAttack = gpGlobals->curtime + 0.25;
	if (m_iClip1 != 0)
		SetWeaponIdleTime( gpGlobals->curtime + 2.5 );
	else
		SetWeaponIdleTime( gpGlobals->curtime + 0.25 );
	m_fInSpecialReload = 0;

	// Update punch angles.
	QAngle angle = pPlayer->GetPunchAngle();

	if ( pPlayer->GetFlags() & FL_ONGROUND )
	{
		angle.x -= SharedRandomInt( "XM1014PunchAngleGround", 3, 5 );
	}
	else
	{
		angle.x -= SharedRandomInt( "XM1014PunchAngleAir", 7, 10 );
	}
	
	pPlayer->SetPunchAngle( angle );
}


bool CWeaponXM1014::Reload()
{
	CCSPlayer *pPlayer = GetPlayerOwner();
	if ( !pPlayer )
		return false;

	if (pPlayer->GetAmmoCount( m_iPrimaryAmmoType ) <= 0 || m_iClip1 == GetMaxClip1())
		return true;

	// don't reload until recoil is done
	if (m_flNextPrimaryAttack > gpGlobals->curtime)
		return true;
	
	//MIKETODO: shotgun reloading (wait until we get content)
	
	// check to see if we're ready to reload
	if (m_fInSpecialReload == 0)
	{
		pPlayer->SetAnimation( PLAYER_RELOAD );

		SendWeaponAnim( ACT_SHOTGUN_RELOAD_START );
		m_fInSpecialReload = 1;
		pPlayer->m_flNextAttack = gpGlobals->curtime + 0.5;
		SetWeaponIdleTime( gpGlobals->curtime + 0.5 );
		m_flNextPrimaryAttack = gpGlobals->curtime + 0.5;
		m_flNextSecondaryAttack = gpGlobals->curtime + 0.5;

#ifdef GAME_DLL
		//TAGGG - whatever... may be better ways, don't know.
		//pPlayer->DoAnimationEvent( PLAYERANIMEVENT_RELOAD_START );
		
		//... I guess?  I don't get how to replace DoAnimationEvent, see weapon_m3.cpp.
		// Even between CS2003 and CS2007, it doesn't seem to actually have any real replacement.
		// DoAnimationEvent is just missing in CS2003 with nothing in its place.  uhh.. nothing here.
#endif

		return true;
	}
	else if (m_fInSpecialReload == 1)
	{
		if (m_flTimeWeaponIdle > gpGlobals->curtime)
			return true;
		// was waiting for gun to move to side
		m_fInSpecialReload = 2;


		//TAGGG - should we use a line like this instead?  because I have no freakin' idea.
		//(from the M3 obviously... even though I have no clue what "M3_RELOAD" was suppoed to be)
		//(I assume a hard-reference to the sequence in the model, but when we use "ACT_VM_RELOAD"
		// we want to let the model decide what sequence to use)
		//SendWeaponAnim( M3_RELOAD, UseDecrement() ? 1: 0);

		SendWeaponAnim( ACT_VM_RELOAD );

		SetWeaponIdleTime( gpGlobals->curtime + 0.5 );


		//TAGGG - fuck you DoAnimationEvent I don't get you.
		/*
#ifdef GAME_DLL
		if ( m_iClip1 == 6 )
		{
			pPlayer->DoAnimationEvent( PLAYERANIMEVENT_RELOAD_END );
		}
		else
		{
			pPlayer->DoAnimationEvent( PLAYERANIMEVENT_RELOAD_LOOP );
		}
#endif
		*/

	}
	else
	{
		// Add them to the clip
		m_iClip1 += 1;
		
		//TAGGG - also no known equivalent.     whatever.
		/*
#ifdef GAME_DLL
		SendReloadEvents();
#endif
		*/
		
		CCSPlayer *pPlayer = GetPlayerOwner();

		if ( pPlayer )
			 pPlayer->RemoveAmmo( 1, m_iPrimaryAmmoType );

		m_fInSpecialReload = 1;
	}
	

	return true;
}


void CWeaponXM1014::WeaponIdle()
{
	CCSPlayer *pPlayer = GetPlayerOwner();
	if ( !pPlayer )
		return;

	if (m_flPumpTime && m_flPumpTime < gpGlobals->curtime)
	{
		// play pumping sound
		m_flPumpTime = 0;
	}

	if (m_flTimeWeaponIdle < gpGlobals->curtime)
	{
		if (m_iClip1 == 0 && m_fInSpecialReload == 0 && pPlayer->GetAmmoCount( m_iPrimaryAmmoType ))
		{
			Reload( );
		}
		else if (m_fInSpecialReload != 0)
		{
			if (m_iClip1 != 7 && pPlayer->GetAmmoCount( m_iPrimaryAmmoType ))
			{
				Reload( );
			}
			else
			{
				// reload debounce has timed out
				//MIKETODO: shotgun anims
				SendWeaponAnim( ACT_SHOTGUN_RELOAD_FINISH );
				
				// play cocking sound
				m_fInSpecialReload = 0;
				SetWeaponIdleTime( gpGlobals->curtime + 1.5 );
			}
		}
		else
		{
			SendWeaponAnim( ACT_VM_IDLE );
		}
	}
}
