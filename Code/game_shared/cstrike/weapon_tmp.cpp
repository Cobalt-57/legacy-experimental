//========= Copyright � 1996-2005, Valve Corporation, All rights reserved. ============//
//
// Purpose: 
//
//=============================================================================//

//TAGGG - file moved from CS2007, absent from CS2003

#include "cbase.h"
#include "weapon_csbasegun.h"


#if defined( CLIENT_DLL )

	#define CWeaponTMP C_WeaponTMP
	#include "c_cs_player.h"

#else

	#include "cs_player.h"

#endif


class CWeaponTMP : public CWeaponCSBaseGun
{
public:
	DECLARE_CLASS( CWeaponTMP, CWeaponCSBaseGun );
	DECLARE_NETWORKCLASS(); 
	DECLARE_PREDICTABLE();
	
	CWeaponTMP();

	virtual void Spawn();

	virtual void PrimaryAttack();

	//TAGGG - we not be doin' that mun
	//virtual CSWeaponID GetWeaponID( void ) const		{ return WEAPON_TMP; }

	virtual bool IsSilenced( void ) const				{ return true; }

private:

	CWeaponTMP( const CWeaponTMP & );

	//TAGGG - added cycletime
	void TMPFire( float flSpread, float flCycleTime );
	void DoFireEffects( void );
};

IMPLEMENT_NETWORKCLASS_ALIASED( WeaponTMP, DT_WeaponTMP )

BEGIN_NETWORK_TABLE( CWeaponTMP, DT_WeaponTMP )
END_NETWORK_TABLE()

BEGIN_PREDICTION_DATA( CWeaponTMP )
END_PREDICTION_DATA()

LINK_ENTITY_TO_CLASS( weapon_tmp, CWeaponTMP );
PRECACHE_WEAPON_REGISTER( weapon_tmp );



CWeaponTMP::CWeaponTMP()
{
}



//TAGGG - CS2003 expects CWeaponCSBaseGun child classes to fill out this info.
void CWeaponTMP::Spawn()
{
	CCSBaseGunInfo info;
	//Filled in from the .txt file as we have it from CS2007 (or later)
	info.m_flAccuracyDivisor = 200;
	info.m_flAccuracyOffset = 0.55;
	info.m_flMaxInaccuracy = 1.4;

	info.m_iPenetration = 1;
	info.m_iDamage = 26;
	info.m_flRangeModifier = 0.84;

	info.m_flTimeToIdleAfterFire = 2.0f;
	info.m_flIdleInterval = 20.0f;

	CSBaseGunSpawn( &info );
}




void CWeaponTMP::PrimaryAttack( void )
{
	CCSPlayer *pPlayer = GetPlayerOwner();
	if ( !pPlayer )
		return;
	
	//TAGGG - added the hardcoded m_flCycleTime to be sent along at this point.
	// (based off of how other CS2003 weapons do this)
	// That is, "GetCSWpnData()" did not have "m_flCycleTime" at this point, so it's hardcoded.
	if ( !FBitSet( pPlayer->GetFlags(), FL_ONGROUND ) )
		TMPFire( 0.25f * m_flAccuracy, 0.07 );
	else
		TMPFire( 0.03f * m_flAccuracy, 0.07 );
}

//TAGGG - accepts this hardcoded cycletime value here.
void CWeaponTMP::TMPFire( float flSpread, float flCycleTime )
{
	//TAGGG - compatible.
	//if ( !CSBaseGunFire( flSpread, GetCSWpnData().m_flCycleTime, true ) )
	if ( !CSBaseGunFire( flSpread, flCycleTime ) )
		return;

	CCSPlayer *pPlayer = GetPlayerOwner();

	// CSBaseGunFire can kill us, forcing us to drop our weapon, if we shoot something that explodes
	if ( !pPlayer )
		return;


	//TAGGG - edito.
	/*
	if ( !FBitSet( pPlayer->GetFlags(), FL_ONGROUND ) )
		pPlayer->KickBack (1.1, 0.5, 0.35, 0.045, 4.5, 3.5, 6);
	else if (pPlayer->GetAbsVelocity().Length2D() > 5)
		pPlayer->KickBack (0.8, 0.4, 0.2, 0.03, 3, 2.5, 7);
	else if ( FBitSet( pPlayer->GetFlags(), FL_DUCKING ) )
		pPlayer->KickBack (0.7, 0.35, 0.125, 0.025, 2.5, 2, 10);
	else
		pPlayer->KickBack (0.725, 0.375, 0.15, 0.025, 2.75, 2.25, 9);
	*/

	if ( !FBitSet( pPlayer->GetFlags(), FL_ONGROUND ) )
		KickBack (1.1, 0.5, 0.35, 0.045, 4.5, 3.5, 6);
	else if (pPlayer->GetAbsVelocity().Length2D() > 5)
		KickBack (0.8, 0.4, 0.2, 0.03, 3, 2.5, 7);
	else if ( FBitSet( pPlayer->GetFlags(), FL_DUCKING ) )
		KickBack (0.7, 0.35, 0.125, 0.025, 2.5, 2, 10);
	else
		KickBack (0.725, 0.375, 0.15, 0.025, 2.75, 2.25, 9);

}

void CWeaponTMP::DoFireEffects( void )
{
	// TMP is silenced, so do nothing
}

