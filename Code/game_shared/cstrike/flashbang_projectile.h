//====== Copyright � 1996-2003, Valve Corporation, All rights reserved. =======
//
// Purpose: 
//
//=============================================================================

#ifndef HEGRENADE_PROJECTILE_H
#define HEGRENADE_PROJECTILE_H
#ifdef _WIN32
#pragma once
#endif


#include "basegrenade_shared.h"


//TAGGG - motherfuck this.

class CFlashbangProjectile : public CBaseGrenade
{
public:
	DECLARE_CLASS( CFlashbangProjectile, CBaseGrenade );
	DECLARE_DATADESC();

	


	//TAGGG - we need to do this apparently?
	// .. no?
//#ifndef CLIENT_DLL
//	DECLARE_SERVERCLASS();
//#endif
	


// Overrides.
public:
	
	virtual void Spawn();
	virtual void Precache();


// Grenade stuff.
public:

	static CFlashbangProjectile* Create( 
		const Vector &position, 
		const QAngle &angles, 
		const Vector &velocity, 
		const AngularImpulse &angVelocity, 
		CBaseCombatCharacter *pOwner, 
		float timer );

	void FlashbangDetonate();

protected:

};










#endif // FLASHBANG_PROJECTILE_H
