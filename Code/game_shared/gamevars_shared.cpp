//========= Copyright � 1996-2005, Valve Corporation, All rights reserved. ============//
//
// Purpose: 
//
// $NoKeywords: $
//=============================================================================//
#include "cbase.h"
#include "gamevars_shared.h"

// memdbgon must be the last include file in a .cpp file!!!
#include "tier0/memdbgon.h"

// some shared cvars used by game rules
ConVar mp_forcecamera( 
	"mp_forcecamera", 
	"0", 
	FCVAR_REPLICATED,
	"Restricts spectator modes for dead players" );
	
ConVar mp_allowspectators(
	"mp_allowspectators", 
	"1.0", 
	FCVAR_REPLICATED,
	"toggles whether the server allows spectator mode or not" );

ConVar friendlyfire(
	"mp_friendlyfire",
	"0",
	FCVAR_REPLICATED | FCVAR_NOTIFY );


//TAGGG - linkerfix.
//nope. we want the CS2003 one (below)
//keep in mind this is just for view, was in one of the _SDK files or whatever.
//cvar_t  allow_spectators = { "allow_spectators", "0.0", FCVAR_SERVER };		// 0 prevents players from being spectators

//well it was in CS2003 too so why not
// some shared cvars used by game rules
ConVar deadspecmode( 
	"mp_deadspecmode", 
	"0", 
	FCVAR_REPLICATED,
	"Restricts spectator modes for dead players" );
//and this is the important one apparently.	
ConVar allow_spectators(
	"allow_spectators", 
	"1.0", 
	FCVAR_REPLICATED,
	"toggles whether the server allows spectator mode or not" );

//////////////////////////////////////////////////////////////////////////////////////////////

